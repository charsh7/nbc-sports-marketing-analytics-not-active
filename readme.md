**NBC Sports Marketing Analytics** is a project to forecast subscriber churn for various NBC Sports Gold passes and provide detailed actionable insights into the drivers that contribute to churn.

## Preparing your system

The following instructions are valid for a Unix-based (Linux, macOS, etc.) or Windows operating system.

1. [Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) this repository to the machine (cloud or local) where you intend to run the project.
2. Install the latest version of the [Anaconda Python Distribution](https://www.anaconda.com/distribution/) using the instructions for your OS.
3. Create a new Python 3.7 virtual environment named nbc_marketing with ```conda create --name nbc_marketing python=3.7``` and activate it with ```conda activate nbc_marketing```.
4. Install the required packages with ```pip install -r requirements.txt```.

## Running the project

In order to forecast churn for the set of desired sports, you must run the ```churn_forecast_all_sports.py``` script in the ```src/``` folder with
```
python churn_forecast_all_sports.py
```

When run for the *first time*, this will do the following 

* Download the raw client-data files/sources (subscriptions, video consumption, email engagement, marketing attribution etc.) from an S3 bucket to ```data_raw/```.
* Run multi-threaded data aggregation tasks on larger data sources (video consumption and email engagement) in order to roll them up to manageable file sizes.
* Prepare the input data for the churn models for each desired sport by engineering several features from the various data sources for each subscriber.
* Train predictive models (Logistic Regression supplemented with XGBoost + SHAP) on past customer churn behavior to predict future churn behavior for each sport.
* Collate the results of the various models into files that can be used to refresh an interactive churn dashboard that the client uses to access the insights produced in a customizable manner and take appropriate actions.

In runs after the first one, only those data prep steps will be performed that are necessiated by the arrival of new data, and those that are set to a *forced run* mode.

