
import logging
import pandas as pd
import dask.dataframe as dd
import gzip
import shutil
import os
import csv

def dask_read_csv(filepath, sep, partitionsize="64MB", 
    nrows=None, force_extract_if_exists=False):
    """A function to read csv files (including those compressed and having a .gz
    extension) into a dask.dataframe.
    """

    # create placeholder for the returns
    ddf = None
    extractedfilepath = filepath
    
    if nrows is None:        
        # extract file if compressed as ".gz"
        file_is_compressed = False
        if filepath.endswith(".gz"):
            file_is_compressed = True
            extractedfilepath = "../extracted_gzip_cache/"\
                + filepath.replace("/", "__").replace('\\', "__")[:-3]
            if not os.path.exists(extractedfilepath) or force_extract_if_exists:
                logging.debug("\nExtracting " + filepath + " ...")
                # create folders in savepath
                if not os.path.exists(os.path.split(extractedfilepath)[0]):
                    os.makedirs(os.path.split(extractedfilepath)[0])
                # extract
                with gzip.open(filepath, "rb") as fin,\
                    open(extractedfilepath, "wb") as fout:
                    #-----
                    shutil.copyfileobj(fin, fout)
                logging.debug("\nExtracted " + filepath + " to " 
                    + extractedfilepath)

        # read in dd
        ddf = dd.read_csv(
            extractedfilepath,
            sep=sep,
            dtype="object",
            blocksize=partitionsize,
            error_bad_lines=False,
            quoting=csv.QUOTE_NONE,
            )

    else: # nrows is not None
        ddf = dd.from_pandas(pd.read_csv(filepath,
            sep=sep,
            nrows=nrows,
            dtype="object",
            error_bad_lines=False,
            quoting=csv.QUOTE_NONE,
            ), npartitions=2)

    return ddf, extractedfilepath