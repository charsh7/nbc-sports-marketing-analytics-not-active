import os
import csv
import pandas as pd
import logging
import gzip
from shutil import rmtree
from time import sleep

def split_csv(filepath, sep=',', partfile_sizeinbytes=1*1024**3, 
    output_path_template='./split_%s.csv', max_files=None, keep_headers=True):

    # print how many parts will be created
    logging.debug("Your file will be split into approximately "
        + str(os.path.getsize(filepath) // partfile_sizeinbytes + 1) 
        + " parts at the path " + output_path_template
        )
    
    # get row_limit
    #
    # read in 1000 rows of file
    sample_1000_rows = pd.read_csv(
        filepath,
        nrows=1000,
        sep=sep,
        error_bad_lines=False,
        )
    #
    # save to disk
    samplepath = "__temp_sample_1000_rows.csv"
    sample_1000_rows.to_csv(samplepath, sep=sep, index=False)
    #
    # get size of sample file and delete
    sample_file_size = os.path.getsize(samplepath)
    try: os.remove(samplepath)
    except: logging.debug("Could not delete " + samplepath)
    #
    # get number of rows needed to reach partfile_sizeinbytes
    row_limit = int(partfile_sizeinbytes\
        * (len(sample_1000_rows) / sample_file_size))
    
    # open filehandler
    reader = None
    if filepath.endswith(".gz"):
        reader = gzip.open(filepath, "rt")
    else:
        reader = open(filepath, "r")

    # create fresh folder
    folderpath = os.path.split(output_path_template)[0]
    if os.path.exists(folderpath):
        rmtree(folderpath)
        sleep(2)
    if not os.path.exists(folderpath):
        os.makedirs(folderpath)
    
    # the splittin'
    #
    headerline = next(reader)
    current_piece = 0
    current_limit = 0
    current_out_writer = None
    #
    for i, row in enumerate(reader):
        #
        if i + 1 > current_limit:
            current_piece += 1
            if (max_files is not None) and (current_piece > max_files):
                break
            current_limit = row_limit * current_piece
            current_out_path = os.path.join(output_path_template  % current_piece)
            logging.debug("Now saving " + current_out_path + " ...")
            current_out_writer = open(current_out_path, 'a')
            current_out_writer.write(headerline)
        #    
        current_out_writer.write(row)
    #
    reader.close()
    current_out_writer.close()

    logging.debug("Splitting done.")