# ************************************************************************************************
# Kaizen Analytix LLC                                                                            #
# Proprietary and Confidential                                                                   #
# Copyright 2019 Kaizen Analytix LLC - All Rights Reserved                                       #
#                                                                                                #
# This program is part of Kaizen Analytix LLC's Kaizen ValueAcceleratorTM commercially licensed  #
# software: Any unauthorized use, imitation, duplication or redistribution of any portion of     #
# this file, via any medium, without the expressed written consent of Kaizen Analytix, is        #
# strictly prohibited. Any violation of these terms may result in additional fees, penalties or  #
# nullification of licenses or agreements pertaining to this program.                            #
# ************************************************************************************************

import subprocess
import pandas as pd

def run_command(command):
    """Run a shell command."""
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    (outputbytes, err) = popen.communicate()
    output = outputbytes.decode("utf-8")
    return output

def sync_s3folder_to_localfolder(s3folderpath, localfolderpath, credentialspath):
    """Function that syncs S3 folder to local folder.
    This will only reliably work from a terminal, not from a Jupyter notebook,
    because in a notebook, subprocess.Popen runs commands in the environment 
    where Jupyter is installed, not the environment of the notebook itself.

    Args:
        s3folderpath (str): path of the S3 folder (source) that you want
            to sync from e.g. "s3://bucketname/path/to/folder"
        localfolderpath (str): path of the local folder (destination) you 
            want to sync to e.g. "./".
        credentialspath (str): local path of .csv containing the bucket access
            credentials.

    Returns:
        None
    """
    
    # read in s3 bucket credentials
    credentials = pd.read_csv(credentialspath).iloc[0]

    # cmds to configure keys in awscli
    run_command("aws configure set aws_access_key_id " 
        + credentials["access_key_id"])
    run_command("aws configure set aws_secret_access_key " 
        + credentials["secret_access_key"])
    
    # cmd to sync data from bucket to local
    return run_command("aws s3 sync " + s3folderpath 
        + " " + localfolderpath)

if __name__ == "__main__":
    # # example run
    # s3folderpath = "s3://ka-nbc/test"
    # localfolderpath = "./"
    # credentialspath = "../config/s3_bucket_credentials.csv"
    # print(sync_s3folder_to_localfolder(
    #     s3folderpath, localfolderpath, credentialspath))

    # full NBC sync
    print("\nSyncing data from S3 ...")
    outputstr = sync_s3folder_to_localfolder(s3folderpath="s3://ka-nbc/incoming/", 
        localfolderpath="../data_raw/",
        credentialspath="../config/s3_bucket_credentials.csv")
    print("\n" + outputstr)
    print("\nSyncing data from S3 complete.")