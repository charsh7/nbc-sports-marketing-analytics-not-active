import logging
from configure_logging import configure_logging
import pandas as pd
from glob import glob
import os
from df_info_str import df_info_str
import sys
from collections import defaultdict
import numpy as np

def save_email_customer_attributes():
    """Compute content consumption and email engagement columns for email customers.
    """

    # print start status
    start_time = pd.Timestamp.now()
    logging.info("\nEmail customers attributes computation started at "
        + str(start_time))

    # getting + saving the clean content consumption of email customers
    ecust_atts = agg_content_emailcustomers()

    # save email consumption for email event category "open"
    #
    # getting support data needed for email engagement calculation
    subkeys = ecust_atts["subkey"].drop_duplicates().values
    date_min, date_max = ecust_atts["date"].min(), ecust_atts["date"].max()
    #
    # compute and save
    save_emailconsumption("open", subkeys, date_min, date_max)

    # print done status
    logging.info("\nEmail customers attributes computation done in "
        + str(pd.Timestamp.now() - start_time))


def agg_content_emailcustomers():
    """Clean all raw content consumption files and concat.
    """
    
    # read in the filenames
    emailcustomers_rawdatafolder = "../data_raw/email_customers_consumption"
    rawfiles = glob(emailcustomers_rawdatafolder + "/*.zip")
    logging.debug("\nCleaning " + str(len(rawfiles)) 
        + " raw emailcustomer content cons. files ...")

    # read in mapping from raw sport to clean sport
    mapping_path = "../config/emailcustomer_videoleague_to_sport.csv"
    rawsport_to_cleansport = defaultdict(lambda: "unknown",
        pd.read_csv(mapping_path).set_index(
            "emailcustomer_videoleague")["sport"]
        )

    # clean the files into dfs
    cleandfs = []
    for rf_ix, rf in enumerate(rawfiles[None:None]):
        try:
            rawdf = pd.read_csv(rf, dtype="object")
            cleandf = agg_content_emailcust_rawdf(rawdf, rawsport_to_cleansport)
            cleandfs.append(cleandf)
        except:
            logging.error("Unable to clean file " + str(rf))
            sys.exit(1)
        
    # concat and agg cleandfs
    logging.debug("\nAgging cleaned content cons. dfs into one df ...")
    cleandf = agg_content_emailcust_cleandf(
        pd.concat(cleandfs, ignore_index=True))
    logging.debug("\ncleandf info:\n\n" + df_info_str(cleandf))

    # save to file
    outpath = "../data_transformed/email_customers/custatts_dateskeylvl.csv"
    if not os.path.exists(os.path.split(outpath)[0]):
        os.makedirs(os.path.split(outpath)[0])
    cleandf.to_csv(outpath, index=False)
    logging.info("\nEmail customers cons. atts saved to " + str(outpath))

    return cleandf

def agg_content_emailcust_rawdf(rawdf, rawsport_to_cleansport):
    """Clean a raw email customer file to get a df with content consumption 
    ("visits" cols for different content types) at a user-date-sport level.
    """

    # initialize "clean" events from a copy of "dirty" rawdf
    events = rawdf.copy()

    # keeping cols of interest
    cols_wantedname_to_rawname = {"subkey": "103. Subscriber Key (evar103)",
        "date": "Date",
        "sport": "17. Video League (evar17)",
        "livestarts": "19. Live Stream Starts (event19)",
        "livesecs": "21. Live Stream Seconds Watched (event21)",
        "ferstarts": "57. Full Event Replay Start (event57)",
        "fersecs": "59. Full Event Replay Seconds Watched (event59)",
        "hlstarts": "7. H1ghlight Clip Starts (event7)",
        "hlsecs": "14. Highlight Clip Seconds Watched (event14)",
        "clipname": "21. Camera Angle / Clip Name (evar21)",
        }
    events = events[list(cols_wantedname_to_rawname.values())]
    events.columns = list(cols_wantedname_to_rawname.keys())

    # subkey: formating and getting rid of null/bad subkey rows
    #
    # lowercase and strip
    events["subkey"] = events["subkey"].str.lower().str.strip()
    #
    # remove nulls and "scriber" subkey rows
    events = events[~(
        events["subkey"].isnull() | (events["subkey"].str.contains("scriber"))
        )]

    # compute "visits" columns
    #
    # convert raw cons (starts/secs) cols into float
    conscols = ["livestarts", "livesecs", "ferstarts", "fersecs", 
        "hlstarts", "hlsecs"]
    for c in conscols:
        events[c] = events[c].astype(float)
    #
    # get "visits_vid_live", "visits_vid_fer", , "visits_vid_hl"
    events["visits_vid_live"] = ((events["livestarts"] > 0) | (events["livesecs"] > 0))*1
    events["visits_vid_fer"] = ((events["ferstarts"] > 0) | (events["fersecs"] > 0))*1
    events["visits_vid_hl"] = ((events["hlstarts"] > 0) | (events["hlsecs"] > 0))*1
    #
    # get "visits_vid_eng":
    # if visits_vid (live, fer, hl) are all 0, but clipname is nonnull
    # visit is type "vid_eng" (engagement)
    events["visits_vid_eng"] = (events["visits_vid_live"] == 0)\
        & (events["visits_vid_fer"] == 0)\
        & (events["visits_vid_hl"] == 0)\
        & (events["clipname"].notnull())
    events["visits_vid_eng"] = events["visits_vid_eng"] * 1
    #
    # get "visits_ed":
    # if visits_vid (all) are 0, visit is type "visit_ed" (editorial)
    events["visits_ed"] = (events["visits_vid_live"] == 0)\
        & (events["visits_vid_fer"] == 0)\
        & (events["visits_vid_hl"] == 0)\
        & (events["visits_vid_eng"] == 0)
    events["visits_ed"] = events["visits_ed"] * 1
    #
    # add up all visit types into visits
    typevisit_cols = events.columns[events.columns.str.startswith("visits_")]
    events["visits"] = events[typevisit_cols].sum(axis="columns")
    #
    # dropping raw consumption columns
    events = events.drop(["livestarts", "livesecs", "ferstarts", "fersecs", 
        "hlstarts", "hlsecs", "clipname"], axis="columns")

    # format date
    events["date"] = pd.to_datetime(events["date"], infer_datetime_format=True)

    # consolidating sports
    events["sport"] = events["sport"].str.lower().str.strip()
    events["sport"] = events["sport"].map(rawsport_to_cleansport)

    # summing to subkey-date-sport level
    events = agg_content_emailcust_cleandf(events)

    return events

def agg_content_emailcust_cleandf(cleandf):
    """Sum clean content consumption df to a user-date-sport level.
    """

    # make copy for changes
    events = cleandf.copy()

    # summing to subkey-date-sport level
    gbcols = ["subkey", "date", "sport"]
    events = events.groupby(gbcols).sum().reset_index()

    return events


def save_emailconsumption(email_event_categ, subkeys, date_min, date_max):
    """Save to file the email consumption df, for a category (click, open etc) 
    for a set of subkeys in a particular daterange.
    """

    # print status
    logging.debug("Getting email " + email_event_categ + " data ...")

    # read and clean the email events (emev) df
    #
    # read raw emev df
    emev_filepaths = glob("../data_transformed/email_customers/"
        "email_agged/" + email_event_categ + "_datelevel_" + "[0-9]"*8 + ".csv")
    logging.debug("\n" + email_event_categ + " files found = " 
        + str(emev_filepaths))
    emevdf = pd.concat([pd.read_csv(p) for p in emev_filepaths], 
                ignore_index=True)
    #
    # format date
    emevdf["date"] = pd.to_datetime(emevdf["date"].astype(str), 
        infer_datetime_format=True)
    #
    # rename subscriberkey col to subkey for compat during join
    emevdf = emevdf.rename(columns={"subscriberkey": "subkey"})

    # restricting to subkeys and date range from content cons.
    emevdf = emevdf[emevdf["subkey"].isin(subkeys)
        & (emevdf["date"] >= date_min)
        & (emevdf["date"] <= date_max)
        ]
    
    # sum to date-subkey level
    emevdf["pianoid"] = emevdf["pianoid"].astype(str)
    emevdf = emevdf.groupby(["date", "subkey", "pianoid"]).sum().reset_index()
    emevdf["pianoid"] = emevdf["pianoid"].replace("nan", np.nan)

    # print emevdf info
    logging.debug("\nemevdf info:\n\n" + df_info_str(emevdf))
    
    # saving
    outpath = "../data_transformed/email_customers/emailcustomer_emailopens.csv"
    if not os.path.exists(os.path.split(outpath)[0]):
        os.makedirs(os.path.split(outpath)[0])
    emevdf.to_csv(outpath, index=False)
    logging.debug("Saved relevant email " + email_event_categ + "s data to " 
        + outpath)

if __name__ == "__main__":
    configure_logging("DEBUG")
    save_email_customer_attributes()
