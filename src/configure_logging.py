# ************************************************************************************************
# Kaizen Analytix LLC                                                                            #
# Proprietary and Confidential                                                                   #
# Copyright 2019 Kaizen Analytix LLC - All Rights Reserved                                       #
#                                                                                                #
# This program is part of Kaizen Analytix LLC's Kaizen ValueAcceleratorTM commercially licensed  #
# software: Any unauthorized use, imitation, duplication or redistribution of any portion of     #
# this file, via any medium, without the expressed written consent of Kaizen Analytix, is        #
# strictly prohibited. Any violation of these terms may result in additional fees, penalties or  #
# nullification of licenses or agreements pertaining to this program.                            #
# ************************************************************************************************

import datetime
import logging

def configure_logging(log_level, log_folder_path=None):
    """Configure logging options.

    Args:
        log_level (str): The logging level e.g. "INFO", "DEBUG" etc.
        log_folder_path (str, None):
            If str, path to folder where you want to save log files.
            If None, print to console.
    
    Returns:
        None
    """

    # determining filename for logging
    logfilename = None
    if log_folder_path is not None:
        timestr = str(datetime.datetime.now()).replace(" ", "_")\
            .replace(":", ".")
        logfilename = log_folder_path + "/" + timestr + ".log"
    
    # configuring the logger
    logging.basicConfig(level=getattr(logging, log_level),
        filename=logfilename,
        format='\n%(asctime)s %(levelname)s: %(message)s')