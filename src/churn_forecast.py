import logging
from configure_logging import configure_logging
import yaml
from df_info_str import df_info_str
from churn_data_prep import churn_data_prep
import pandas as pd
import os
import numpy as np
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import StandardScaler
from xgboost import XGBClassifier
from sklearn.metrics import confusion_matrix, precision_score
import shap
from sklearn.linear_model import LogisticRegression
from scipy.special import expit
import os

# read in the configuration options
with open("../config/churn_forecast_config.yaml", 'r') as f:
    CONFIG = yaml.safe_load(f)

def churn_forecast(sportcode, forecastseason_namepart=None,
    force_dataprep=False, saveoutput=True):
    """Generate the churn forecast for a given sportcode.

    Args:
        sportcode (str): "sportcode" of the sport we want to forecast
        forecastseason_namepart (str): Partial name of the season we want 
            to forecast. If None, use latest season with sufficient history.
        force_dataprep (bool): If True, force the data_prep step

    Returns:
        None

    Files needed:
        - ../data_transformed/churn_input/churn_input_SPORTCODE.csv

    Files created:
        - ../data_transformed/churn_forecast/churn_forecast_SPORTCODE.csv
        - ../data_transformed/churn_forecast/sportinfo_SPORTCODE.csv
    """

    # Started forecast status
    logging.info("\nChurn forecast started for sport: " + sportcode)

    # read in the samples that will act as input to the churn model
    samples = read_churn_input(sportcode, force_dataprep)

    # get forecast and model samples
    if sportcode != "PGAT":
        samples_forec, samples_model = get_forecast_and_model_samples(
            forecastseason_namepart, samples)
    else: # sportcode == "PGAT":
        samples_forec, samples_model = get_forecast_and_model_samples_pgat(samples)

    # perform feature selection for model samples
    featcols, featcols_valid, x_sets, y_sets = select_features(sportcode, samples_model)

    # standardize data in each season
    x_sets, binarycols = standardize_x_sets(x_sets)

    # get forecast xs and standardize
    xs_forec_stz = get_xs_forec_stz(samples_forec, featcols, binarycols)

    # combine and split model sets into train and test sets
    xs_train_stz, ys_train_smote, xs_test_stz, ys_test =\
        get_train_test_sets(x_sets, y_sets)

    # dropping features with low predictive power
    featcols, xs_train_featskeep, xs_test_featskeep, xs_forec_featskeep =\
        shap_select_features(
            xs_train_stz, ys_train_smote, xs_test_stz, ys_test, xs_forec_stz)

    # creating a series to store sport-level info: sportinfo
    #
    # getting the current churn rate
    churn_rate_current = None
    if sportcode != "PGAT":
        churn_rate_current = y_sets[-1].sum() / y_sets[-1].count()
    else: # sportcode == "PGAT"
        # use second last season for completeness
        churn_rate_current = y_sets[-2].sum() / y_sets[-2].count()
    #
    sportinfo = pd.Series({
        "sport_season_nm": samples_forec["sport_season_nm"].iloc[0],
        "churn_rate_current": churn_rate_current
        })

    # train model
    model, coefs, feats_validnotinmodel, sportinfo = train_model(
        xs_train_featskeep, ys_train_smote, xs_test_featskeep, ys_test, 
        featcols_valid, sportinfo)
    
    # generate forecast
    make_forecast(model, coefs, xs_forec_featskeep, samples_forec, 
        feats_validnotinmodel, sportinfo, sportcode, saveoutput)

    # # save feature contributions for the latest test set
    # save_featcons_for_train_samples(model, samples_model, featcols, binarycols,
    #     sportcode)

    # Started forecast status
    logging.info("\nChurn forecast done for sport: " + sportcode)

def read_churn_input(sportcode, force_dataprep):
    """Read churn input file for a given sportcode

    Args:
        sportcode (str): "sportcode" of the sport we want to forecast
        force_dataprep (bool): If True, force the data_prep step 

    Returns:
        samples (pandas.DataFrame): Table containing user attributes for each season, one
        row per season-user
    """

    logging.debug("\nStarted reading churn input file ...")
    churninputpath = "../data_transformed/churn_input/churn_input_" + sportcode\
        + ".csv"
    if not os.path.exists(churninputpath) or force_dataprep:
        churn_data_prep(sportcode)
    samples = pd.read_csv(churninputpath)
    logging.debug("\nsamples (churn input) info:\n\n" + df_info_str(samples))
    return samples

def get_forecast_and_model_samples(forecastseason_namepart, samples):
    """Determine the season that season we will be forecasting churn for and 
    the season we will train the model on. The model_season will be the one that preceeds
    the forecast_season.
    """
    
    logging.debug("\nGetting model and forecast seasons ...")

    # get list of seasons
    seasons = samples["sport_season_nm"].drop_duplicates().sort_values()\
        .reset_index(drop=True)
    logging.debug("\nseasons:\n" + str(seasons))

    # get forecast season
    #
    # assume we are forecasting for the latest season by default
    forecseas_rowix = None
    #
    # use forecastseason_namepart if provided
    if forecastseason_namepart is not None:
        forecseas_rowix = seasons.index[
            seasons.str.contains(forecastseason_namepart)].values[0]
    #
    # if forecastseason_namepart not provided, 
    # choose most recent season with sufficient history (time elapsed)
    else:
        # set to last season by default
        forecseas_rowix = seasons.index[-1]
        #
        # switch to second-last season if insufficient history in lastseason
        forec_seas_min_frac_elapsed = CONFIG["forec_seas_min_frac_elapsed"]
        days_in_season = 365
        samples_latest_season = samples[
            samples["sport_season_nm"] == seasons.loc[forecseas_rowix]]
        latest_subdt = samples_latest_season["subscription_start_dt"].max()
        latest_season_startdt = samples_latest_season["season_start_date"].iloc[0]
        seas_frac_elapsed = (pd.to_datetime(latest_subdt)\
            - pd.to_datetime(latest_season_startdt)).days / days_in_season
        logging.debug("\nLatest season fraction elapsed = " + str(seas_frac_elapsed))
        if seas_frac_elapsed < forec_seas_min_frac_elapsed:
            forecseas_rowix = seasons.index[-2]

    season_to_forecast = seasons.loc[forecseas_rowix]
    logging.debug("\nseason_to_forecast = " + season_to_forecast)
    samples_forec = samples[samples["sport_season_nm"] == season_to_forecast]

    # get model season
    n_seasons_to_model = CONFIG["n_seasons_to_model"]
    seasons_to_model = seasons.values[forecseas_rowix - n_seasons_to_model
        : forecseas_rowix]
    logging.debug("\nseasons_to_model = " + str(seasons_to_model))
    samples_model = samples[samples["sport_season_nm"].isin(seasons_to_model)]

    return samples_forec, samples_model

def get_forecast_and_model_samples_pgat(samples):
    """Active users will make up the forecast samples.
    """
    
    logging.debug("\nGetting model and forecast samples ...")

    # get date of last sub
    last_sub_date = samples["subscription_start_dt"].max()

    # get min sub date for forecasting
    days_in_season = 31
    grace_days_to_renew = CONFIG["pgat_grace_days_to_renew"]
    forecast_start_date = str(pd.to_datetime(last_sub_date) - pd.to_timedelta(
        str(days_in_season + grace_days_to_renew - 1) + " days"))[None:10]

    # get forecast samples
    samples_forec = samples[
        samples["subscription_start_dt"] >= forecast_start_date].copy()
    samples_forec = samples_forec.drop_duplicates(["user_id"], keep="last")

    # give descriptive sport_season_nm to forecast samples
    forecast_seasonname = "PGATL Monthly (" + forecast_start_date\
        +  " to " + last_sub_date + ")"
    samples_forec["sport_season_nm"] = forecast_seasonname
    logging.debug("\nseason_to_forecast = " + forecast_seasonname)

    # get model samples
    #
    samples_model = samples[
        samples["subscription_start_dt"] < forecast_start_date].copy()

    # limit seasons in the model samples
    n_seasons_to_model = CONFIG["pgat_n_months_to_model"]
    #
    # get season names
    model_seasonnames = samples_model["sport_season_nm"].drop_duplicates()\
        .sort_values().values
    #
    # kick out last season from consideration if very short (e.g. < 21 days) 
    # (due to most of it being in the forecast samples)
    forecast_dayofmonth = int(forecast_start_date[-2:None])
    if forecast_dayofmonth < CONFIG["pgat_train_month_min_days"]:
        model_seasonnames = model_seasonnames[None:-1]
    #
    # select seasons
    model_seasonnames_keep = model_seasonnames[-n_seasons_to_model:None]
    samples_model = samples_model[
        samples_model["sport_season_nm"].isin(model_seasonnames_keep)]
    logging.debug("\nseasons_to_model = " + str(model_seasonnames_keep))

    return samples_forec, samples_model

def select_features(sportcode, samples_model):

    logging.debug("\nSelecting features ...")

    # breaking up samples into season sets
    sample_sets = []
    seasons = samples_model["sport_season_nm"].drop_duplicates().sort_values()\
        .values
    for season in seasons:
        sample_sets.append(samples_model[
            samples_model["sport_season_nm"] == season])

    # extract targets (ys) and features (xs)
    targetcol = "will_churn"
    logging.debug("\ntargetcol = " + targetcol)
    featcols = sorted(set(samples_model.columns) - set([targetcol]))
    logging.debug("\nfeatcols (all) = " + str(featcols))
    xs = samples_model[featcols]
    x_sets, y_sets = [], []
    for ss in sample_sets:
        x_sets.append(ss[featcols])
        y_sets.append(ss[targetcol])
        
    # keeping only specific price cols
    pricecols = xs.columns[xs.columns.str.contains("price")]
    pricecols_keep = ["price_charged_now_to_first_time"]
    if sportcode == "PGAT":
        # remove all price related info because of fixed price
        pricecols_keep = []
    pricecols_drop = sorted(set(pricecols) - set(pricecols_keep))
    featcols = sorted(set(featcols) - set(pricecols_drop))
    xs = xs[featcols]
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = x_set[featcols]
    logging.debug("\nunwanted price featcols dropped = " + str(pricecols_drop))
        
    # keeping only specific price cols
    pricecols = xs.columns[xs.columns.str.contains("price")]
    pricecols_keep = ["price_charged_now_to_first_time"]
    if sportcode == "PGAT":
        # remove all price related info because of fixed price
        pricecols_keep = []
    pricecols_drop = sorted(set(pricecols) - set(pricecols_keep))
    featcols = sorted(set(featcols) - set(pricecols_drop))
    xs = xs[featcols]
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = x_set[featcols]
    logging.debug("\nunwanted price featcols dropped = " + str(pricecols_drop))

    # exclude emailsents cols from the model
    emailsentscols = xs.columns[xs.columns.str.contains("emailsents")]
    featcols = sorted(set(featcols) - set(emailsentscols))
    logging.debug("\nunwanted emailsents featcols dropped = " + str(emailsentscols))


    # drop featcols with non-numerical data
    xs_std = xs.std()
    featcols_drop = sorted(set(featcols) - set(xs_std.index))
    featcols = sorted(xs_std.index)
    xs = xs[featcols]
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = x_set[featcols]
    logging.debug("\nnon-num featcols dropped = " + str(featcols_drop))

    # saving names of featcols valid for modeling
    featcols_valid = featcols.copy()
    logging.debug("\nfeatcols (valid for the model) = " + str(featcols_valid))

     # drop featcols with 0/null variance
    featcols_drop = xs_std.index[(xs_std == 0) | xs_std.isnull()]
    featcols = sorted(set(featcols) - set(featcols_drop))
    xs = xs[featcols]
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = x_set[featcols]
    logging.debug("\n0/null variance featcols dropped = " + str(list(featcols_drop)))

    # dropping feats with low activity (rowvals > 0)
    min_activity = CONFIG["min_activity"]
    feat_activities = (xs > 0).sum() / xs.count()
    featcols_drop = feat_activities.index[feat_activities < min_activity]
    featcols = sorted(set(featcols) - set(featcols_drop))
    xs = xs[featcols]
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = x_set[featcols]
    logging.debug("\nlow activity (<" + str(min_activity) + ") featcols dropped = "
        + str(list(featcols_drop)))
    logging.debug("\nfeatcols (selected) = " + str(featcols))

    # drop extra collinear features in PGAT
    if sportcode == "PGAT":
        featcols_drop = ["seasons_watched", "videosecs"]
        featcols = sorted(set(featcols) - set(featcols_drop))
        xs = xs[featcols]
        for set_ix, x_set in enumerate(x_sets):
            x_sets[set_ix] = x_set[featcols]
        logging.debug("\nextra collinear featcols dropped = "
            + str(list(featcols_drop))
            + "\nfeatcols (selected) = " + str(featcols))

    # impute missing
    xs = xs_impute_missing(xs)
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = xs_impute_missing(x_set)

    return featcols, featcols_valid, x_sets, y_sets

def xs_impute_missing(xs):
    # replace infs with nulls
    xs = xs.replace([np.inf, -np.inf], np.nan)
    
    # replace nulls with column-means
    xs = xs.fillna(xs.mean())

    return xs

def standardize_x_sets(x_sets):

    logging.debug("\nStandardizing x_sets.")

    # identifying binary and continuous columns
    binarycols = []
    for col in x_sets[-1].columns:
        if set(x_sets[-1][col].astype(float).unique()) == set([0., 1.]):
            binarycols.append(col)
    logging.debug("binarycols = " + str(binarycols))

    # scale the x_sets
    for set_ix, x_set in enumerate(x_sets):
        x_sets[set_ix] = standardize_xs(x_set, binarycols)

    return x_sets, binarycols

def standardize_xs(xs, binarycols):

    # get binary and continuous cols
    binarycols = set(xs.columns).intersection(binarycols)
    contcols = set(xs.columns) - set(binarycols)

    # make copy to change
    xs_stz = xs.copy()

    # scale binary cols to [-1 , 1]
    xs_stz.loc[:, binarycols] = (xs[binarycols] - 0.5)/0.5

    # standard scale the continuous cols
    xs_stz.loc[:, contcols] = \
        StandardScaler().fit_transform(xs[contcols])

    return xs_stz

def get_xs_forec_stz(samples_forec, featcols, binarycols):
    xs_forec = xs_impute_missing(samples_forec[featcols])
    xs_forec_stz = standardize_xs(xs_forec, binarycols)
    logging.debug("\nlen(xs_forec_stz) = " + str(len(xs_forec_stz)))
    return xs_forec_stz

def get_train_test_sets(x_sets, y_sets):

    # combine sets
    xs = pd.concat(x_sets, ignore_index = True)
    ys = pd.concat(y_sets, ignore_index = True)

    # train test split
    test_size = CONFIG["test_size"]
    xs_train, xs_test, ys_train, ys_test = train_test_split(
        xs, ys, test_size=test_size, random_state=42)
    logging.debug("\nlen(xs_train) = " + str(len(xs_train)) 
        + ", len(xs_test) = " + str(len(xs_test)))

    # resample
    #
    # pick sampling strategy
    # get samples in each class up to a minimum number
    logging.debug("\nResampling data ...")
    sampling_strategy = "auto"
    minsamplesperclasswanted = eval(CONFIG["minsamplesperclasswanted"])
    n_majorityclassamples = ys_train.value_counts().max()
    if n_majorityclassamples < minsamplesperclasswanted:
        sampling_strategy = \
            dict([(label, minsamplesperclasswanted) for label in set(ys_train)])
    # smote
    xs_train_smote, ys_train_smote = \
        SMOTE(sampling_strategy=sampling_strategy, random_state=42)\
        .fit_sample(xs_train.fillna(0), ys_train)
    #
    # give col names
    xs_train_smote = pd.DataFrame(xs_train_smote, 
        columns=list(xs_train))
    ys_train_smote = pd.Series(ys_train_smote, name=ys_train.name)
    logging.debug("\nys_train_smote.value_counts():\n" 
        + str(ys_train_smote.value_counts()))

    # rename xs__smote to xs__stz for compat with later steps
    xs_train_stz = xs_train_smote
    xs_test_stz = xs_test

    return xs_train_stz, ys_train_smote, xs_test_stz, ys_test

def shap_select_features(xs_train_stz, ys_train_smote, xs_test_stz, ys_test, 
    xs_forec_stz):

    logging.debug("\nDropping features with low shap (predictive power) ...")

    # train and predict XGB model
    model = XGBClassifier(n_estimators = 100, n_jobs = -1)
    model.fit(xs_train_stz, ys_train_smote)
    ys_pred = model.predict(xs_test_stz)

    # performance
    acc_y1, acc_y0, acc_avgclasswise, prec = get_churn_model_accuracies(
        ys_test, ys_pred)
    logging.debug("\nXGB model (for getting shaps) performance:"
        + '\naccuracy among churns = ' + str(round(acc_y1, 2))
        + '\naccuracy among non-churns = ' + str(round(acc_y0, 2))
        + '\nAVERAGE class-wise accuracy = ' + str(round(acc_avgclasswise, 2))
        + '\nprecision for churners = ' + str(round(prec, 2)))

    # compute shaps
    explainer = shap.TreeExplainer(model)
    shaps = pd.DataFrame(explainer.shap_values(xs_train_stz),
        columns=xs_train_stz.columns)
    feature_shaps = shaps.abs().mean().sort_values(ascending=False)
    logging.debug("\nfeature_shaps:\n" + str(feature_shaps))

    # keep only features with shap > minshap (predictive power)
    minshap = CONFIG["minshap"]
    featcols_drop = feature_shaps.index[feature_shaps <= minshap]
    featcols = sorted(set(xs_train_stz.columns) - set(featcols_drop))
    xs_train_featskeep = xs_train_stz[featcols]
    xs_test_featskeep = xs_test_stz[featcols]
    xs_forec_featskeep = xs_forec_stz[featcols]
    logging.debug("\nlow shap (<= " + str(minshap) + ") featcols dropped = " 
        + str(list(featcols_drop)))
    logging.debug("\nfeatcols = " + str(featcols))

    return featcols, xs_train_featskeep, xs_test_featskeep, xs_forec_featskeep

def get_churn_model_accuracies(ys_test, ys_pred):
    cm = confusion_matrix(ys_test, ys_pred)
    acc_y1 = cm[1,1] / cm.sum(1)[1]
    acc_y0 = cm[0,0] / cm.sum(1)[0]
    acc_avgclasswise = (acc_y0 + acc_y1)/2
    prec = precision_score(y_true=ys_test, y_pred=ys_pred)
    return acc_y1, acc_y0, acc_avgclasswise, prec

def train_model(xs_train_featskeep, ys_train_smote, xs_test_featskeep, ys_test, 
    featcols_valid, sportinfo):

    logging.debug("\nTraining model ...")

    # train and predict
    model = LogisticRegression(C=CONFIG["logistic_c_param"])
    model.fit(xs_train_featskeep, ys_train_smote)
    ys_predproba = model.predict_proba(xs_test_featskeep)[:, 1]
    predthresh = 0.5
    ys_pred = (ys_predproba > predthresh) * 1

    # measuring model performance
    sportinfo["accuracy_churners_test"],\
        sportinfo["accuracy_nonchurners_test"],\
        sportinfo["accuracy_avgclasswise_test"],\
        sportinfo["precision_churners_test"] = get_churn_model_accuracies(
            ys_test, ys_pred)
    logging.debug("\naccuracy_churners_test = " 
        + str(round(sportinfo["accuracy_churners_test"], 2))
        + "\naccuracy_nonchurners_test = "
        + str(round(sportinfo["accuracy_nonchurners_test"], 2))
        + "\naccuracy_avgclasswise_test = "
        + str(round(sportinfo["accuracy_avgclasswise_test"], 2))
        + '\nprecision_churners_test = '
        + str(round(sportinfo["precision_churners_test"], 2))
        )

    # get feature impacts
    #
    # get coefs
    coefs = pd.Series(model.coef_[0], index=xs_train_featskeep.columns)
    #
    # sort by absolute value
    coefs = coefs[coefs.abs().sort_values(ascending=False).index]
    
    # # SAVE COEFS!
    # coefs_and_intercept = coefs.copy()
    # coefs_and_intercept["intercept"] = model.intercept_[0]
    # outpath = "../data_transformed/churn_forecast/coefs_"\
    #     + sportcode + ".csv"
    # if not os.path.exists(os.path.split(outpath)[0]): 
    #     os.makedirs(os.path.split(outpath)[0])
    # pd.DataFrame(coefs_and_intercept).transpose().to_csv(outpath, index=False)

    # convert coefs to overall feature impacts
    featimpacts = expit(coefs + model.intercept_[0]) - expit(model.intercept_[0])
    featimpacts["intercept"] = expit(model.intercept_[0])
    logging.debug("\nfeatimpacts:\n" + str(featimpacts))
    #
    # add featimpacts to sportinfo
    feats_validnotinmodel = set(featcols_valid) - set(xs_train_featskeep.columns)
    featimpacts_allvalid = pd.concat([featimpacts,
        pd.Series([0]*len(feats_validnotinmodel), index=feats_validnotinmodel)
        ])
    featimpacts_allvalid.sort_index(inplace=True)
    featimpacts_allvalid.index = "impact_" + featimpacts_allvalid.index
    sportinfo = pd.concat([sportinfo, featimpacts_allvalid])

    return model, coefs, feats_validnotinmodel, sportinfo

def make_forecast(model, coefs, xs_forec_featskeep, samples_forec, 
    feats_validnotinmodel, sportinfo, sportcode, saveoutput):

    logging.debug("\nForecasting ...")

    # initializing the result table (forecdf) with all feature values
    #
    featnames = samples_forec.columns[
        ~samples_forec.columns.isin(["will_churn"])]
    forecdf = samples_forec[featnames].set_index("user_id")
    forecdf.columns = "featval_" + forecdf.columns
    #
    # replacing infs with nans
    forecdf = forecdf.replace([np.inf, -np.inf], np.nan)

    # predicting ys_forec
    ys_forec_predprob = model.predict_proba(xs_forec_featskeep)[:, 1]
    forecdf["churn_prob"] = ys_forec_predprob
    ys_forec_pred = (ys_forec_predprob > 0.5) * 1
    sportinfo["churn_rate_predicted"] = ys_forec_pred.sum() / len(ys_forec_pred)

    # save sportinfo to file
    if saveoutput:
        outpath = "../data_transformed/churn_forecast/sportinfo_"\
            + sportcode + ".csv"
        if not os.path.exists(os.path.split(outpath)[0]): 
            os.makedirs(os.path.split(outpath)[0])
        pd.DataFrame(sportinfo).transpose().to_csv(outpath, index=False)
    logging.debug("\nsportinfo info:\n\n" + str(sportinfo))

    # Getting the churn probability contributions of features
    featcons_forec = get_shap_probs(model, xs_forec_featskeep)
    featcons_forec.columns = "contrib_" + featcons_forec.columns
    featcons_forec.index = forecdf.index
    forecdf = forecdf.join(featcons_forec)

    # adding contributions of excluded features as 0
    for f in feats_validnotinmodel:
        forecdf["contrib_" + f] = 0
        
    # churn driver cols
    #
    # getting churn driver among all drivers
    feats_all = set(featcons_forec.columns) - set(["contrib_intercept"])
    forecdf = add_churndriver_col("all", feats_all, forecdf)
    #
    # creating driver categories
    featcats_d = {"email": ["emailclicks_acq", "emailclicks_eng", 
            "emailclicks_other", "emailopens_acq", "emailopens_eng", 
            "emailopens_other"],
        "marketing_attribution": ["mktg_channel_email", "mktg_channel_pr", 
            "mktg_channel_search", "mktg_channel_social", "mktg_channel_web", 
            "mktg_channel_exists"],
        "operational": ["is_multisport", "price_charged_now_to_first_time", 
            "season_days_elapsed", "seasons_watched", "tenure"],
        "video": ["videosecs", "videosecs_connected", "videosecs_mobile",  
            "videosecs_web", "videosecs_live", "videosecs_replay"]
        }
    #
    # adding churn driver for each category
    for featcat, catfeatcols in featcats_d.items():
        concols = "contrib_" + pd.Series(catfeatcols)
        forecdf = add_churndriver_col(featcat, concols, forecdf)
        
    ## retention and churn driver cols
    #
    # getting retention driver among all drivers
    feats_all = set(featcons_forec.columns) - set(["contrib_intercept"])
    forecdf = add_retentiondriver_col("all", feats_all, forecdf)
    #
    # adding churn driver for each category
    for featcat, catfeatcols in featcats_d.items():
        concols = "contrib_" + pd.Series(catfeatcols)
        forecdf = add_retentiondriver_col(featcat, concols, forecdf)
        
    # sorting col names
    forecdf = forecdf[sorted(forecdf.columns)]

    # save forecdf to file
    if saveoutput:
        outpath = "../data_transformed/churn_forecast/churn_forecast_"\
            + sportcode + ".csv"
        if not os.path.exists(os.path.split(outpath)[0]): 
            os.makedirs(os.path.split(outpath)[0])
        forecdf.to_csv(outpath)
    logging.debug("\nforecdf info:\n\n" + df_info_str(forecdf))

def get_shap_probs(model, xs):
    
    # compute predictions
    y_probs = model.predict_proba(xs)[:, 1]

    # get shaps
    explainer = shap.LinearExplainer(model, xs, 
        feature_dependence="independent")
    shap_values = explainer.shap_values(xs)
    shap_base = explainer.expected_value

    # transform shap_base to prob
    shap_base_prob = expit(shap_base)

    # distance from expected prob to pred prob per sample
    d_probs = y_probs - shap_base_prob

    # shap distance (sum of shaps) per sample
    d_shaps = shap_values.sum(axis=1)

    # scale factors per sample
    scalefactors = (d_probs / d_shaps).reshape((-1, 1))

    # transforming shaps to probs
    shap_probs = shap_values * scalefactors
    
    # converting array to df
    shap_prob_df = pd.DataFrame(shap_probs, columns=xs.columns)
    shap_prob_df["intercept"] = shap_base_prob
    
    return shap_prob_df

def add_churndriver_col(featcat, concols, forecdf):
    """Function for finding churn drivers in specific category (featcat)"""
    
    # focusing on concols that exist
    concols = sorted(set(forecdf.columns).intersection(set(concols)))
    
    # valid rows and their drivers
    valid_rows = (forecdf[concols] > 0).any(axis="columns")\
        & (forecdf["churn_prob"] > 0.5)
    valid_row_drivers = forecdf.loc[valid_rows][concols].idxmax(axis="columns")
    
    # adding churndriver col for featcat
    forecdf["churndriver_" + featcat] = None
    forecdf.loc[valid_rows, "churndriver_" + featcat] = valid_row_drivers
    return forecdf

def add_retentiondriver_col(featcat, concols, forecdf):
    """Function for finding retention drivers in specific categoty (featcat)"""
    
    # focusing on concols that exist
    concols = sorted(set(forecdf.columns).intersection(set(concols)))
    
    # valid rows and their drivers
    valid_rows = (forecdf[concols] < 0).any(axis="columns")\
        & (forecdf["churn_prob"] < 0.5)
    valid_row_drivers = forecdf.loc[valid_rows][concols].idxmin(axis="columns")
    
    # adding retentiondriver col for featcat
    forecdf["retentiondriver_" + featcat] = None
    forecdf.loc[valid_rows, "retentiondriver_" + featcat] = valid_row_drivers
    return forecdf

def churn_forecast_many_sports(sportcode_to_forecastseason_namepart,
    force_dataprep=False):
    """Perform churn forecast for many sports. See docstring of churn_forecast
    for more info.
    
    Args:
    sportcode_to_forecastseason_namepart (pandas.DataFrame): Dataframe where each
        row corresponds to a sport. Columns are "sportcode" and 
        "forecastseason_namepart".
    force_dataprep (bool): If True, force the data prep step

    Returns:
    None
    """

    sportcodes = sportcode_to_forecastseason_namepart["sportcode"].values

    # print start time
    start_time = pd.Timestamp.now()
    logging.info("\nChurn forecast for sports " + str(sportcodes) 
        + " started at " + str(start_time))

    for _, sportrow in sportcode_to_forecastseason_namepart.iterrows():
        churn_forecast(sportrow["sportcode"], sportrow["forecastseason_namepart"],
            force_dataprep)

    # print end time
    logging.info("\nChurn forecast for sports " + str(sportcodes) 
        + " done in " + str(pd.Timestamp.now() - start_time))

def save_featcons_for_train_samples(model, samples_model, featcols, binarycols,
    sportcode):

    # get last season samples
    samples_lastmodelseason = samples_model[
        samples_model["sport_season_nm"] == samples_model["sport_season_nm"].max()]

    # get features and target
    xs = samples_lastmodelseason[featcols]
    ys = samples_lastmodelseason["will_churn"]

    # initializing df to store results
    preddf = xs.copy()
    preddf["will_churn_actual"] = ys

    # standardize xs
    xs_stz = get_xs_forec_stz(xs, featcols, binarycols)

    # get predictions
    ys_predprob = model.predict_proba(xs_stz)[:, 1]
    ys_pred = (ys_predprob > 0.5)*1
    #
    preddf["will_churn_pred"] = ys_pred
    preddf["will_churn_predprob"] = ys_predprob

    # get misclassification status
    misclass_flags = ((ys_pred - ys) != 0)*1
    #
    preddf["misclass_flag"] = misclass_flags

    # Getting the churn probability contributions of features
    featcons = get_shap_probs(model, xs_stz)
    featcons.columns = "contrib_" + featcons.columns
    #
    featcons.index = preddf.index
    preddf = preddf.join(featcons)

    # save
    outpath = "../data_transformed/churn_forecast/train_preds_"\
        + sportcode + ".csv"
    if not os.path.exists(os.path.split(outpath)[0]): 
        os.makedirs(os.path.split(outpath)[0])
    preddf.to_csv(outpath, index = False)

if __name__ == "__main__":
    configure_logging("DEBUG")

    # sportcode = "PGAT"
    # churn_forecast(sportcode, saveoutput=False)
    
    sportcode = "Track&Field"
    churn_forecast(sportcode, saveoutput=False)

    # sportcodes = ["PGAT", "Track&Field"]
    # sportcode_to_forecastseason_namepart = pd.DataFrame({
    #     "sportcode": sportcodes, 
    #     "forecastseason_namepart": [None]*len(sportcodes)
    #     })
    # churn_forecast_many_sports(sportcode_to_forecastseason_namepart)