import logging
from configure_logging import configure_logging
from glob import glob
from dask.distributed import Client
from dask_read_csv import dask_read_csv
from df_info_str import df_info_str
from find_if_input_files_newer_than_output import\
    find_if_input_files_newer_than_output
import os
import pandas as pd
import dask.dataframe as dd
from collections import defaultdict
from itertools import chain
import numpy as np

def email_agg(delete_extracts_when_done=True):

    # note start time
    start_time = pd.Timestamp.now()
    logging.info("\nEmail agg started at " + str(start_time))

    # get refresh folder names
    refreshfolders = list(sorted(glob(
        "../data_raw/wingspan_scorecard/" + "[0-9]"*8)))
    logging.debug("\nrefreshfolders = " + str(refreshfolders))

    # get subkey_to_piano mapping
    subkey_to_piano = get_subkey_to_piano(refreshfolders)

    # get sendid_to_type mapping
    sendid_to_type = get_sendid_to_type(refreshfolders)

    # agg refresh folders sequentially
    for ix, refreshfolder in enumerate(refreshfolders[None:None]):
        previousrefreshfolder = None
        if ix > 0:
            previousrefreshfolder = refreshfolders[ix - 1]
        agg_refreshfolder(refreshfolder, subkey_to_piano, sendid_to_type,
            previousrefreshfolder, delete_extracts_when_done)

    # note time taken
    logging.info("\nEmail agg done in " + str(pd.Timestamp.now() - start_time))

def get_subkey_to_piano(refreshfolders):
    
    logging.debug("\nMapping subkey to piano ...")

    # combine subkey to piano mappings from all refresh folders
    subkey_to_piano = None
    for rf_ix, rf in enumerate(refreshfolders):
        
        # get the scorecard filename
        scardfile = list(filter(
            lambda p: "emailscorecard" in p.lower(),
            glob(rf + "/*.?sv*")
            ))[0]
        logging.debug("\nGetting subkey_to_piano from " + str(scardfile) + " ...")

        # get path of mapping
        rf_mapping_path = "../data_transformed/email_engagement/"\
            "subkey_to_piano/subkey_to_piano_" + rf[-8:] + ".csv"

        # save mapping to file if needed
        if find_if_input_files_newer_than_output([scardfile], [rf_mapping_path]):
            
            # read in file
            sep = ("\t" if ".tsv" in scardfile else ",")
            scard_df = pd.read_csv(scardfile, sep=sep, nrows=None)

            # standardize colnames
            scard_df.columns = scard_df.columns.str.replace(" ", "")\
                .str.lower()

            # keep relevant columns
            colnames_keep = ["subscriptionid", "pianoid"]
            colnames_new = ["subscriberkey", "pianoid"]
            scard_df = scard_df[colnames_keep]
            scard_df.columns = colnames_new
            
            # drop null piano rows
            scard_df = scard_df.dropna()

            # lowercase and strip all cols
            for cn in scard_df.columns:
                scard_df[cn] = scard_df[cn].str.lower().str.strip()

            # drop duplicates
            scard_df = scard_df.drop_duplicates(subset=["subscriberkey"])

            # remove existing subkeys
            if rf_ix > 0:
                existing_subkeys = set(subkey_to_piano["subscriberkey"])
                scard_df = scard_df[
                    ~(scard_df["subscriberkey"].isin(existing_subkeys))]

            # save to file
            if not os.path.exists(os.path.split(rf_mapping_path)[0]):
                os.makedirs(os.path.split(rf_mapping_path)[0])
            scard_df.to_csv(rf_mapping_path, index=False)

        # read mapping from file
        rf_mapping = pd.read_csv(rf_mapping_path)

        # add to final mapping
        if rf_ix == 0:
            subkey_to_piano = rf_mapping
        else: # rf_ix > 0
            subkey_to_piano = pd.concat([subkey_to_piano, rf_mapping], 
                ignore_index=True)

    logging.debug("\nsubkey_to_piano info:\n\n" + df_info_str(subkey_to_piano))

    return subkey_to_piano

def get_sendid_to_type(refreshfolders):
    """Map sendid to (type, brand, sport)"""
    
    logging.debug("\nMapping sendid to (type, brand, sport) ...")
    
    # read sendjobs and emailtrackinginfo
    sendjobdf = read_sendjobs(refreshfolders)
    emtrackdf = read_emailtrackinginfo(refreshfolders)

    # merge to get sendid_to_type df
    sendid_to_type = sendjobdf.merge(emtrackdf, how="inner")
    sendid_to_type = sendid_to_type.drop(["emailname"], axis="columns")

    # map email type to typebucket
    #
    # typepart to typebucket mapping
    typepart_to_typebucket_raw = pd.read_csv(
        "../config/email_typepart_to_typebucket.csv")\
        .set_index("typepart")["typebucket"].to_dict()
    typepart_to_typebucket = defaultdict(
        lambda: typepart_to_typebucket_raw["<default>"],
        typepart_to_typebucket_raw)
    #
    # function to get the matching namepart from a name
    def contained_namepart(name, nameparts):
        match = None
        matchflags = nameparts.map(lambda namepart: namepart in name)
        if True in matchflags.values:
            match = nameparts[matchflags].iloc[0]
        return match
    #
    # map type to typebucket (under "type" column name)
    typeparts = pd.Series(list(typepart_to_typebucket))
    sendid_to_type["type"] = sendid_to_type.groupby("type")["type"]\
        .transform(lambda g: typepart_to_typebucket[
            contained_namepart(g.name, typeparts)])

    # mapping emailtracking sportcode to subscription sportcode
    #
    # dict class that returns key if missing
    class DictMissing2Self(dict):
        def __missing__(self, key):
            return key
    #
    # read in the mapping df
    emsportcode_to_subsportcode_df = pd.read_csv("../config/"
        + "emailtracking_sportcode_to_subscription_sportcode.csv")
    #
    # create the mapping dict that returns key (sport) if missing
    emsportcode_to_subsportcode = DictMissing2Self(
        emsportcode_to_subsportcode_df.set_index("emailtracking_sportcode")
            ["subscription_sportcode"].to_dict()
        )
    #
    # performing the mapping on sendid_to_type
    sendid_to_type["sportcode"] = sendid_to_type["sport"].map(
        emsportcode_to_subsportcode)
    sendid_to_type = sendid_to_type.drop(["sport"], axis="columns")
    logging.debug("\nsendid_to_type info:\n\n" + df_info_str(sendid_to_type))

    return sendid_to_type

def read_sendjobs(refreshfolders):

    # get paths of files with required name
    sendjobfiles = list(filter(
        lambda p: "sendjobs" in p.lower(),
        chain(*map(lambda rf: glob(rf + "/*.?sv*"), refreshfolders))
        ))
    logging.debug("\nsendjobfiles = " + str(sendjobfiles))

    # read sendjobfiles into dfs
    sendjob_dfs = []
    for p in sendjobfiles:
        #
        # determining separator
        sep = ("\t" if ".tsv" in p else ",")
        #
        # reading
        sendjob_dfs.append(pd.read_csv(
            p,
            sep = sep,
            error_bad_lines=False,
            dtype="object"
            ))
        
    # keep relevant columns
    #
    def getrelevantcolumns_sendjobdf(df):
        """Get relevant columns from sendjob df"""
        
        # make shallow copy to avoid SettingWihCopy warning
        df = df.copy()

        # standardize colnames
        df.columns = df.columns.str.replace(" ", "")\
            .str.lower()

        # keep relevant columns
        colnames_keep = ["sendid", "emailname"]
        df = df[colnames_keep]
        
        # lowercase and strip all cols
        for cn in df.columns:
            df[cn] = df[cn].str.lower().str.strip()
        
        return df
    #
    sendjob_cleandfs = list(map(getrelevantcolumns_sendjobdf, sendjob_dfs))

    # concat the sendjob dfs
    sendjobdf = pd.concat(reversed(sendjob_cleandfs)).drop_duplicates(["sendid"])

    return sendjobdf

def read_emailtrackinginfo(refreshfolders):

    # get paths of files with required name
    emtrackfiles = list(filter(
        lambda p: "sfmc_email" in p.lower(),
        chain(*map(lambda rf: glob(rf + "/*.?sv*"), refreshfolders))
        ))
    logging.debug("\nemtrackfiles = " + str(emtrackfiles))

    # read emtrackfiles into dfs
    emtrack_dfs = []
    for p in emtrackfiles:
        #
        # determining separator
        sep = ("\t" if ".tsv" in p else ",")
        #
        # reading
        emtrack_dfs.append(pd.read_csv(
            p,
            sep=sep,
            error_bad_lines=False,
            dtype="object"
            ))
        
    # keep relevant columns
    #
    def getrelevantcolumns_emtrackdf(df):
        """Get relevant columns from emtrack df"""
        
        # make shallow copy to avoid SettingWihCopy warning
        df = df.copy()

        # standardize colnames
        df.columns = df.columns.str.replace(" ", "")\
            .str.lower()

        # keep relevant columns
        colnames_keep = ["nametouse", "type", "brand", "sport"]
        df = df[colnames_keep]
        df = df.rename({"nametouse": "emailname"}, axis="columns")
        
        # lowercase and strip all cols
        for cn in df.columns:
            df[cn] = df[cn].str.lower().str.strip()
        
        return df
    #
    emtrack_cleandfs = list(map(getrelevantcolumns_emtrackdf, emtrack_dfs))

    # concat the emtrack dfs
    emtrackdf = pd.concat(reversed(emtrack_cleandfs)).drop_duplicates(
        ["emailname"])

    return emtrackdf

def agg_refreshfolder(refreshfolder, subkey_to_piano, sendid_to_type,
    previousrefreshfolder, delete_extracts_when_done):
    
    logging.debug("Agging refreshfolder " + refreshfolder + " ...")
    
    eventtypes = ["open", "click", "sent"]
    for eventtype in eventtypes:
        agg_eventtype(eventtype, refreshfolder, subkey_to_piano, sendid_to_type,
            previousrefreshfolder, delete_extracts_when_done)

def agg_eventtype(eventtype, refreshfolder, subkey_to_piano, sendid_to_type,
    previousrefreshfolder, delete_extracts_when_done):

    # get eventtype file
    print("\nrefreshfolder = " + str(refreshfolder))
    eventfile = list(filter(lambda p: eventtype in p.lower(),
        glob(refreshfolder + "/*.tsv*")))[0]

    # print start time
    start_time = pd.Timestamp.now()
    logging.debug("Agging file " + eventfile + " started at " + str(start_time))

    # get agged output file path
    aggpath = "../data_transformed/email_engagement/" + eventtype\
        + "_datelevel_" + refreshfolder[-8:None] + ".csv"

    # don't agg if already agged
    if not find_if_input_files_newer_than_output([eventfile], [aggpath]):
        logging.debug("\nNothing new to agg for refreshfolder = " + refreshfolder 
        + ", eventtype = " + eventtype)
        return

    # start dask client
    daskclient = Client(processes=False)
    logging.debug("\ndaskclient = " + str(daskclient))

    # read eventfile into ddf
    event_ddf, extractedfilepath = dask_read_csv(eventfile, sep="\t",
        partitionsize="64MB")

    # standardize colnames
    event_ddf.columns = event_ddf.columns.str.replace(" ", "")\
        .str.lower()

    # keep relevant columns
    colnames_keep = ["sendid", "subscriberkey", "eventdateid"]
    colnames_new = ["sendid", "subscriberkey", "date"]
    event_ddf = event_ddf[colnames_keep]
    event_ddf.columns = colnames_new

    # remove dates overlapping with previous refresh
    if previousrefreshfolder is not None:
        prevrefaggpath = "../data_transformed/email_engagement/" + eventtype\
            + "_datelevel_" + previousrefreshfolder[-8:None] + ".csv"
        prevref_maxdate = str(
            pd.read_csv(prevrefaggpath)["date"].max())
        logging.debug("\nprevref_maxdate = " + prevref_maxdate)
        event_ddf = event_ddf[event_ddf["date"] > prevref_maxdate]

    # lowercase and strip all cols
    for cn in event_ddf.columns:
        event_ddf[cn] = event_ddf[cn].str.lower().str.strip()

    # getting (type, brand, sport) for sendid
    event_ddf = event_ddf.merge(sendid_to_type, how="inner")
    event_ddf = event_ddf.drop(["sendid"], axis="columns")

    # getting piano for subscriberkey
    event_ddf = event_ddf.merge(subkey_to_piano, how="left")

    # assigning the n_events column
    event_ddf["n_events"] = 1

    # grouping by all cols except n_events and summing
    #
    groupbycolnames = ['date', 'subscriberkey', 'pianoid', 'type', 'brand', 
        'sportcode']
    #
    # converting groupby cols to str to avoid dropping nans
    event_ddf[groupbycolnames] = event_ddf[groupbycolnames].astype(str)
    #
    event_ddf = event_ddf.groupby(groupbycolnames).sum().reset_index()
    event_ddf = event_ddf.replace("nan", np.nan)

    # compute result as df
    event_df = event_ddf.compute()

    # close dask client
    daskclient.close()

    # drop last date data (due to incompleteness of data on that date)
    event_df = event_df[event_df["date"] < event_df["date"].max()]

    # save as file
    if not os.path.exists(os.path.split(aggpath)[0]):
        os.makedirs(os.path.split(aggpath)[0])
    event_df.to_csv(aggpath, index=False)
    logging.debug("\nevent_df (agged) info:\n\n" + df_info_str(event_df))

    # delete extracted file if needed
    if delete_extracts_when_done and "extracted_gzip_cache" in extractedfilepath:
        try: os.remove(extractedfilepath)
        except: logging.debug("\nCound not delete " + extractedfilepath)

    logging.debug("Agging file " + eventfile + " done in "
        + str(pd.Timestamp.now() - start_time))

if __name__ == "__main__":
    configure_logging("DEBUG")
    email_agg(delete_extracts_when_done=False)