import logging
from configure_logging import configure_logging
import pandas as pd
from df_info_str import df_info_str
import numpy as np
import os

def get_custatts_skeylvl(startdate, enddate, inlier_max_n_sigmas):

    # read in emailcustomer date-subkey-level attributes table
    logging.debug("\nReading in emailcustomer date-subkey-level "
        "attributes table ...")
    custatts = pd.read_csv("../data_transformed/email_customers/"
        "custatts_dateskeylvl.csv")
    logging.debug("\ndf_info_str(custatts date-subkey-level):\n\n" 
        + df_info_str(custatts))

    # narrow down to the daterange of interest
    custatts_period = custatts[(custatts["date"] >= startdate)
        & (custatts["date"] <= enddate)]

    # keep only "returning" subkeys:
    # those that existed before period or entered in first 30 days of period
    #
    logging.debug("\nKeep only 'returning' subkeys: "
        "those that existed before period or entered in first 30 days of "
        "period ...")
    #
    # get all subkeys in period
    subkeys_period = custatts_period["subkey"].drop_duplicates().values
    logging.debug("\nlen(subkeys_period) = " + str(len(subkeys_period)))
    #
    # getting eligible subkeys
    maxentrydate = str(pd.to_datetime(custatts_period["date"].min()) 
        + pd.to_timedelta("30 days"))[:10]
    subkeys_prior = custatts[custatts["date"] < maxentrydate]\
        ["subkey"].drop_duplicates().values
    #
    # intersection
    subkeys_ret = set(subkeys_period).intersection(set(subkeys_prior))
    logging.debug("\nlen(subkeys_ret) = " + str(len(subkeys_ret)))
    #
    # restrict custatts_period to returning customers
    custatts_ret = custatts_period[custatts_period["subkey"].isin(subkeys_ret)]

    # aggregate out sport (select sport in future) and date
    visitcols = list(custatts_ret.columns[
        custatts_ret.columns.str.contains("visit")])
    catts_skeylvl = custatts_ret.groupby(["subkey"])[visitcols].sum()\
        .reset_index()

    # bring in email opens for the subkeys
    #
    # read in email opens data
    logging.debug("\nBring in email opens for the subkeys ...")
    opensdf = pd.read_csv("../data_transformed/email_customers/"
        "emailcustomer_emailopens.csv")
    #
    # keep only dates in period
    opensdf = opensdf[(opensdf["date"] >= startdate)
        & (opensdf["date"] <= enddate)]
    #
    # sum to subkey level
    # (turn pianoid col to str during groupby to avoid dropping nans)
    opensdf["pianoid"] = opensdf["pianoid"].astype(str)
    opens_skeylvl = opensdf.groupby(["subkey", "pianoid"])["n_events"].sum()\
        .reset_index()
    opens_skeylvl["pianoid"] = opens_skeylvl["pianoid"].replace("nan", np.nan)
    #
    # set junk pianoids to null
    opens_skeylvl.loc[:, "pianoid"] = opens_skeylvl["pianoid"]\
        .replace(r"\n", np.nan)
    #
    # rename col n_events to emailopens
    opens_skeylvl = opens_skeylvl.rename(columns={"n_events": "emailopens"})
    #
    # join with catts_skeylvl
    #
    catts_skeylvl = catts_skeylvl.merge(opens_skeylvl, how="left")
    #
    # set nulls to 0
    catts_skeylvl.loc[:, "emailopens"] = catts_skeylvl["emailopens"]\
        .replace(np.nan, 0)

    # remove outliers
    logging.debug("\nRemove outliers ...")
    feats = ["visits", "emailopens"]
    subkeys_outlier = set()
    for feat in feats:
        subkeys_outlier = subkeys_outlier.union(set(
            get_outlier_subkeys_along_feature(
                catts_skeylvl, feat, inlier_max_n_sigmas)
            ))
    logging.debug("\nlen(subkeys_all) = " + str(len(catts_skeylvl))
        + "\nlen(subkeys_outlier) = " + str(len(subkeys_outlier)))
    catts_skeylvl = catts_skeylvl[~(catts_skeylvl["subkey"]
        .isin(subkeys_outlier))]

    # Bucket customers by emailopens
    logging.debug("\nBucket customers by emailopens ...")
    bin_edges = [-1, 10, 20, 50, 500]
    catts_skeylvl["emailopens_bucket"] = pd.cut(
        catts_skeylvl["emailopens"], bin_edges).astype(str)
    logging.debug('\ncatts_skeylvl["emailopens_bucket"].value_counts():\n' 
        + str(catts_skeylvl["emailopens_bucket"].value_counts()
            .sort_index())
        )

    # save
    outpath = "../data_transformed/email_customers/"\
        "custatts_skeylvl_" + startdate.replace("-", "") + "to"\
        + enddate.replace("-", "") + ".csv"
    logging.debug("\nSaving to " + outpath + " ...")
    if not os.path.exists(os.path.dirname(outpath)):
        os.makedirs(os.path.dirname(outpath))
    catts_skeylvl.to_csv(outpath, index=False)
    logging.debug("\ndf_info_str(catts_skeylvl):\n\n" 
        + df_info_str(catts_skeylvl))

def get_outlier_subkeys_along_feature(catts_skeylvl, feat, inlier_max_n_sigmas):
    feat_mean = catts_skeylvl[feat].mean()
    feat_std = catts_skeylvl[feat].std()
    subkeys_outlier = catts_skeylvl[catts_skeylvl[feat] 
        > feat_mean + inlier_max_n_sigmas*feat_std]["subkey"].values
    logging.debug("\nlen(subkeys_outlier_" + feat + ") = " 
        + str(len(subkeys_outlier)))
    return subkeys_outlier

if __name__ == "__main__":
    configure_logging("DEBUG")
    get_custatts_skeylvl(startdate="2019-07-01", enddate = "2019-08-31",
        inlier_max_n_sigmas=2)