import logging
from configure_logging import configure_logging
from glob import glob
from dask.distributed import Client
from dask_read_csv import dask_read_csv
from df_info_str import df_info_str
from find_if_input_files_newer_than_output import\
    find_if_input_files_newer_than_output
import os
import pandas as pd
import dask.dataframe as dd
import numpy as np

def email_agg(delete_extracts_when_done=True):

    # note start time
    start_time = pd.Timestamp.now()
    logging.info("\nEmail agg started at " + str(start_time))

    # get refresh folder names
    refreshfolders = list(sorted(glob(
        "../data_raw/wingspan_scorecard/" + "[0-9]"*8)))
    logging.debug("\nrefreshfolders = " + str(refreshfolders))

    # get subkey_to_piano mapping
    subkey_to_piano = get_subkey_to_piano(refreshfolders)

    # get subkeys and daterange we want to keep (from emailcustomers)
    emailcustomersfile = "../data_transformed/email_customers/"\
        "emailcustomer_attributesets.csv"
    ecdf = pd.read_csv(emailcustomersfile)
    subkeys_keep = set(ecdf["subkey"].drop_duplicates())

    # agg refresh folders sequentially
    for ix, refreshfolder in enumerate(refreshfolders[None:None]):
        previousrefreshfolder = None
        if ix > 0:
            previousrefreshfolder = refreshfolders[ix - 1]
        agg_refreshfolder(refreshfolder, subkey_to_piano, subkeys_keep,
            previousrefreshfolder, delete_extracts_when_done)

    # note time taken
    logging.info("\nEmail agg done in " + str(pd.Timestamp.now() - start_time))

def get_subkey_to_piano(refreshfolders):
    
    logging.debug("\nMapping subkey to piano ...")

    # combine subkey to piano mappings from all refresh folders
    subkey_to_piano = None
    for rf_ix, rf in enumerate(refreshfolders):
        
        # get the scorecard filename
        scardfile = list(filter(
            lambda p: "emailscorecard" in p.lower(),
            glob(rf + "/*.*sv*")
            ))[0]
        logging.debug("\nGetting subkey_to_piano from " + str(scardfile) + " ...")

        # get path of mapping
        rf_mapping_path = "../data_transformed/email_customers/email_agged/"\
            "subkey_to_piano/subkey_to_piano_" + rf[-8:] + ".csv"

        # save mapping to file if needed
        if find_if_input_files_newer_than_output([scardfile], [rf_mapping_path]):
            
            # read in file
            sep = ("\t" if ".tsv" in scardfile else ",")
            scard_df = pd.read_csv(scardfile, sep=sep, nrows=None)

            # standardize colnames
            scard_df.columns = scard_df.columns.str.replace(" ", "")\
                .str.lower()

            # keep relevant columns
            colnames_keep = ["subscriptionid", "pianoid"]
            colnames_new = ["subscriberkey", "pianoid"]
            scard_df = scard_df[colnames_keep]
            scard_df.columns = colnames_new
            
            # drop null piano rows
            scard_df = scard_df.dropna()

            # lowercase and strip all cols
            for cn in scard_df.columns:
                scard_df[cn] = scard_df[cn].str.lower().str.strip()

            # drop duplicates
            scard_df = scard_df.drop_duplicates(subset=["subscriberkey"])

            # remove existing subkeys
            if rf_ix > 0:
                existing_subkeys = set(subkey_to_piano["subscriberkey"])
                scard_df = scard_df[
                    ~(scard_df["subscriberkey"].isin(existing_subkeys))]

            # save to file
            if not os.path.exists(os.path.split(rf_mapping_path)[0]):
                os.makedirs(os.path.split(rf_mapping_path)[0])
            scard_df.to_csv(rf_mapping_path, index=False)

        # read mapping from file
        rf_mapping = pd.read_csv(rf_mapping_path)

        # add to final mapping
        if rf_ix == 0:
            subkey_to_piano = rf_mapping
        else: # rf_ix > 0
            subkey_to_piano = pd.concat([subkey_to_piano, rf_mapping], 
                ignore_index=True)

    logging.debug("\nsubkey_to_piano info:\n\n" + df_info_str(subkey_to_piano))

    return subkey_to_piano

def agg_refreshfolder(refreshfolder, subkey_to_piano, subkeys_keep,
    previousrefreshfolder, delete_extracts_when_done):
    
    logging.debug("Agging refreshfolder " + refreshfolder + " ...")
    
    eventtypes = ["open"]
    for eventtype in eventtypes:
        agg_eventtype(eventtype, refreshfolder, subkey_to_piano, subkeys_keep,
            previousrefreshfolder, delete_extracts_when_done)

def agg_eventtype(eventtype, refreshfolder, subkey_to_piano, subkeys_keep,
    previousrefreshfolder, delete_extracts_when_done):

    # get eventtype file
    eventfile = list(filter(lambda p: eventtype in p.lower(),
        glob(refreshfolder + "/*.tsv*")))[0]

    # print start time
    start_time = pd.Timestamp.now()
    logging.debug("Agging file " + eventfile + " started at " + str(start_time))

    # get agged output file path
    aggpath = "../data_transformed/email_customers/email_agged/" + eventtype\
        + "_datelevel_" + refreshfolder[-8:None] + ".csv"

    # don't agg if already agged
    if not find_if_input_files_newer_than_output([eventfile], [aggpath]):
        logging.debug("\nNothing new to agg for refreshfolder = " + refreshfolder 
        + ", eventtype = " + eventtype)
        return

    # start dask client
    daskclient = Client(processes=False)
    logging.debug("\ndaskclient = " + str(daskclient))

    # read eventfile into ddf
    event_ddf, extractedfilepath = dask_read_csv(eventfile, sep="\t",
        partitionsize="64MB")

    # standardize colnames
    event_ddf.columns = event_ddf.columns.str.replace(" ", "")\
        .str.lower()

    # keep relevant columns
    colnames_keep = ["subscriberkey", "eventdateid"]
    colnames_new = ["subscriberkey", "date"]
    event_ddf = event_ddf[colnames_keep]
    event_ddf.columns = colnames_new

    # drop rows with nans
    event_ddf = event_ddf.dropna()

    # remove dates overlapping with previous refresh
    if previousrefreshfolder is not None:
        prevrefaggpath = "../data_transformed/email_customers/email_agged/" + eventtype\
        	+ "_datelevel_" + previousrefreshfolder[-8:None] + ".csv"
        prevref_maxdate = str(
            pd.read_csv(prevrefaggpath)["date"].max())
        logging.debug("\nprevref_maxdate = " + prevref_maxdate)
        event_ddf = event_ddf[event_ddf["date"] > prevref_maxdate]

    # lowercase and strip all cols
    for cn in event_ddf.columns:
        event_ddf[cn] = event_ddf[cn].str.lower().str.strip()

    # keeping only desired subkeys
    event_ddf = event_ddf[event_ddf["subscriberkey"].isin(subkeys_keep)]

    # getting piano for subscriberkey
    event_ddf = event_ddf.merge(subkey_to_piano, how="left")

    # assigning the n_events column
    event_ddf["n_events"] = 1

    # grouping by all cols except n_events and summing
    #
    groupbycolnames = ['date', 'subscriberkey', 'pianoid']
    #
    # converting groupby cols to str to avoid dropping nans
    event_ddf[groupbycolnames] = event_ddf[groupbycolnames].astype(str)
    #
    event_ddf = event_ddf.groupby(groupbycolnames).sum().reset_index()
    event_ddf = event_ddf.replace("nan", np.nan)

    # compute result as df
    event_df = event_ddf.compute()

    # close dask client
    daskclient.close()

    # drop last date data (due to incompleteness of data on that date)
    event_df = event_df[event_df["date"] < event_df["date"].max()]

    # save as file
    if not os.path.exists(os.path.split(aggpath)[0]):
        os.makedirs(os.path.split(aggpath)[0])
    event_df.to_csv(aggpath, index=False)
    logging.debug("\nevent_df (agged) info:\n\n" + df_info_str(event_df))

    # delete extracted file if needed
    if delete_extracts_when_done and "extracted_gzip_cache" in extractedfilepath:
        try: os.remove(extractedfilepath)
        except: logging.debug("\nCound not delete " + extractedfilepath)

    logging.debug("Agging file " + eventfile + " done in "
        + str(pd.Timestamp.now() - start_time))

if __name__ == "__main__":
    configure_logging("DEBUG")
    email_agg()