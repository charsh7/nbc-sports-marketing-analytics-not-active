import io

def df_info_str(df):
    """Returns df.info() + df.head() as a string."""

    # getting df.info()
    buf = io.StringIO()
    df.info(buf=buf)
    info_str = buf.getvalue()

    # getting df.head()
    head_str = str(df.head())

    return "info():\n" + info_str + "head():\n" + head_str