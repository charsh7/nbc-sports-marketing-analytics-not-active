# ************************************************************************************************
# Kaizen Analytix LLC                                                                            #
# Proprietary and Confidential                                                                   #
# Copyright 2019 Kaizen Analytix LLC - All Rights Reserved                                       #
#                                                                                                #
# This program is part of Kaizen Analytix LLC's Kaizen ValueAcceleratorTM commercially licensed  #
# software: Any unauthorized use, imitation, duplication or redistribution of any portion of     #
# this file, via any medium, without the expressed written consent of Kaizen Analytix, is        #
# strictly prohibited. Any violation of these terms may result in additional fees, penalties or  #
# nullification of licenses or agreements pertaining to this program.                            #
# ************************************************************************************************

 # Purpose: This script runs the end-to-end forecast process from raw data 
 # to dashboard input.

# set logging options
#
# choosing the logging level e.g. "INFO", "DEBUG" etc. (default: "INFO")
LOG_LEVEL = "DEBUG"
#
# choosing the logging output format (default: "../logs"):
#    If None, print to console.
#    If str, path to folder where you want to save log files e.g. ("../logs").
LOG_FOLDER_PATH = "../logs"
LOG_FOLDER_PATH = None

import logging
from configure_logging import configure_logging
import pandas as pd
from churn_data_prep import churn_data_prep
from churn_forecast import churn_forecast_many_sports
from make_dashboard_input import make_dashboard_input
import os

#################
# START EXECUTION
#################

# set logging
configure_logging(log_level=LOG_LEVEL, log_folder_path=LOG_FOLDER_PATH)

# note start time
start_time = pd.Timestamp.now()
logging.info("\nEnd-to-end churn forecast for all sports started at " 
    + str(start_time))

# make this directory the working directory
os.chdir(os.path.split(os.path.realpath(__file__))[0])

# read in the sportcode and season namepart for each sport we want to 
# forecast churn for. 
# Where season namepart is absent, the latest season with sufficient history is used.
sportforecseasons_path = "../config/sport_to_forecastseason.csv"
sportforecseasons = pd.read_csv(sportforecseasons_path)
sportforecseasons["forecastseason_namepart"] =\
    sportforecseasons["forecastseason_namepart"].mask(
        lambda nameparts: nameparts.isnull(), None)

# dataprep
sportcodes = list(sportforecseasons["sportcode"].values)
churn_data_prep(sportcodes)

# forecast
churn_forecast_many_sports(sportforecseasons)

# make dashboard input files
make_dashboard_input()

# note end time
logging.info("\nEnd-to-end churn forecast for all sports done in " 
    + str(pd.Timestamp.now() - start_time))