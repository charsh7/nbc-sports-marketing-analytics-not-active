# ************************************************************************************************
# Kaizen Analytix LLC                                                                            #
# Proprietary and Confidential                                                                   #
# Copyright 2019 Kaizen Analytix LLC - All Rights Reserved                                       #
#                                                                                                #
# This program is part of Kaizen Analytix LLC's Kaizen ValueAcceleratorTM commercially licensed  #
# software: Any unauthorized use, imitation, duplication or redistribution of any portion of     #
# this file, via any medium, without the expressed written consent of Kaizen Analytix, is        #
# strictly prohibited. Any violation of these terms may result in additional fees, penalties or  #
# nullification of licenses or agreements pertaining to this program.                            #
# ************************************************************************************************

from glob import glob
import pandas as pd
import os
from shutil import copyfile
import numpy as np
from df_info_str import df_info_str
import logging
from configure_logging import configure_logging

def make_dashboard_input():
    """Combine the churn forecast outputs into consolidated files that populate
    the dashboard. Creates 3 files:
        - ../data_transformed/dashboard_input/featuresinfo.csv
        - ../data_transformed/dashboard_input/featureimpacts_combined.csv
        - ../data_transformed/dashboard_input/churn_forecast_combined.csv
    """

    # print process started status
    logging.info("\nDashboard input creation started")

    # format and combine sportinfo files
    #
    # get sportinfo paths
    sportinfo_paths = glob("../data_transformed/churn_forecast/sportinfo_*.csv")
    #
    # func to format sportinfos into a df with one row per feature impact
    def sportinfo_to_featureimpactdf(sportinfo_path):
        # read in sportinfo
        sportinfo = pd.read_csv(sportinfo_path)
        #
        # take impact cols and make them into rows
        impactnames = sportinfo.columns[sportinfo.columns.str.startswith("impact_")]
        impacts = pd.DataFrame(sportinfo[impactnames].iloc[0])
        impacts.index = impacts.index.str[7:None]
        impacts.index.name = "feature"
        impacts.columns = ["feature_impact"]
        #
        # put other sportinfo metrics as columns
        nonimpactnames = set(sportinfo.columns) - set(impactnames)
        for name, val in sportinfo[nonimpactnames].iloc[0].iteritems():
            impacts[name] = val
        #
        return impacts
    #
    # get featimpactdfs and concat
    featimpacts_combined = pd.concat(
        [sportinfo_to_featureimpactdf(p) for p in sportinfo_paths])\
        .reset_index()
    #
    # copy the featuresinfo.csv file to the dashboard_input/ folder
    outputdir = "../data_transformed/dashboard_input/"
    if not os.path.exists(outputdir): os.makedirs(outputdir)
    copyfile("../config/featuresinfo.csv", outputdir + "/featuresinfo.csv")
    #
    # get the displayname and driver category of each driver
    featuresinfo = pd.read_csv(outputdir + "/featuresinfo.csv")\
        [["feature", "feature_category", "feature_displayname"]]
    featimpacts_combined = featimpacts_combined.merge(featuresinfo, how="left")
    #
    # sort colnames
    featimpacts_combined.sort_index(axis="columns", inplace=True)
    #
    # save to file
    featimpacts_combined.to_csv(outputdir + "/featureimpacts_combined.csv",
        index=False)
    logging.debug("\nfeatimpacts_combined info:\n\n"
        + df_info_str(featimpacts_combined))
        
    # combine churn_forecast files
    #
    # read and concat files
    sport_paths = [
        p for p in glob("../data_transformed/churn_forecast/churn_forecast_*.csv")\
        if "_combined" not in p]
    forecast_combined = pd.concat([pd.read_csv(p) for p in sport_paths],
                         ignore_index=True)
    #
    # setting missing impacts/contribs to 0
    impact_contrib_cols = forecast_combined.columns[
        forecast_combined.columns.str.contains("impact_|contrib_")]
    forecast_combined[impact_contrib_cols] = forecast_combined[impact_contrib_cols]\
        .fillna(0)
    #
    # save
    save_path = "../data_transformed/dashboard_input/churn_forecast_combined.csv"
    forecast_combined.to_csv(save_path, index=False)
    logging.debug("\nforecast_combined info:\n\n" + df_info_str(forecast_combined))

    # print process started status
    logging.info("\nDashboard input creation done")

if __name__ == "__main__":
    configure_logging("DEBUG")
    make_dashboard_input()