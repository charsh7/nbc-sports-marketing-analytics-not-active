# ************************************************************************************************
# Kaizen Analytix LLC                                                                            #
# Proprietary and Confidential                                                                   #
# Copyright 2019 Kaizen Analytix LLC - All Rights Reserved                                       #
#                                                                                                #
# This program is part of Kaizen Analytix LLC's Kaizen ValueAcceleratorTM commercially licensed  #
# software: Any unauthorized use, imitation, duplication or redistribution of any portion of     #
# this file, via any medium, without the expressed written consent of Kaizen Analytix, is        #
# strictly prohibited. Any violation of these terms may result in additional fees, penalties or  #
# nullification of licenses or agreements pertaining to this program.                            #
# ************************************************************************************************

import logging
from configure_logging import configure_logging
from glob import glob
import os
import pandas as pd
from aws_sync import sync_s3folder_to_localfolder
from video_agg import video_agg
from email_agg import email_agg
import re
import numpy as np
from collections import defaultdict
from df_info_str import df_info_str
from find_if_input_files_newer_than_output import\
    find_if_input_files_newer_than_output

def churn_data_prep(sportcodes):
    """Churn data prep for specified sportcodes.

    Args:
        sportcodes (list of str): sportcodes of the sports we want to process

    Returns:
        None

    Files needed:
    <placeholder>

    Files created:
        - ../data_transformed/churn_input/churn_input_SPORTCODE0.csv
        - ../data_transformed/churn_input/churn_input_SPORTCODE1.csv
        ...
    """

    # ensure arg sportcodes is list
    assert (isinstance(sportcodes, list)),\
        "Arg `sportcodes` must be a list of strings."

    # print start time
    start_time = pd.Timestamp.now()
    logging.info("\nChurn data prep for sports " + str(sportcodes) 
        + " started at " + str(start_time))

    # sync raw data from s3
    sync_data_from_s3()
    
    # read sportseason to seasondate mapping
    sportseasondates = read_sportseasondates()
    
    # read marketing attributions
    markatts = read_markatts()
    
    # read video metrics
    vidmetrics = read_vidmetrics()
    
    # read email metrics
    clicks, opens, sents = read_emailmetrics()

    # run data prep for one sport at a time
    for sportcode in sportcodes:

        # read in subs for sportcode
        subs = read_subs(sportcode)

        # prep
        prep_for_sport_after_datasources_loaded(sportcode=sportcode, subs=subs,
            sportseasondates=sportseasondates, markatts=markatts, 
            vidmetrics=vidmetrics, clicks=clicks, opens=opens, sents=sents)

    # print end time
    logging.info("\nChurn data prep for sports " + str(sportcodes) 
        + " done in " + str(pd.Timestamp.now() - start_time))


def sync_data_from_s3():
    # start status
    start_time = pd.Timestamp.now()
    logging.info("Syncing data from S3 started at " + str(start_time) + " ...")

    # sync
    outputstr = sync_s3folder_to_localfolder(s3folderpath="s3://ka-nbc/incoming/", 
        localfolderpath="../data_raw/",
        credentialspath="../config/s3_bucket_credentials.csv")
    logging.debug("\n" + outputstr)
    
    # end status
    logging.info("\nSyncing data from S3 done in " 
        + str(pd.Timestamp.now() - start_time))

def read_subs(sportcode):

    logging.debug("\nReading raw subs for sportcode: " + str(sportcode) + " ...")

    # read subs
    paths = glob("../data_raw/subscriptions/" + sportcode + ".xlsx")
    subs = pd.concat([pd.read_csv(p, sep = "\t", 
        parse_dates=["SUBSCRIPTION_START_DT"],
        infer_datetime_format=True) for p in paths], ignore_index=True)
    logging.debug("\nsubs info:\n\n" + df_info_str(subs))
    
    # save combined subs to file for inspection in Excel
    outpath = "../data_transformed/subscriptions"\
        + "/subscriptions_" + sportcode + ".csv"
    if find_if_input_files_newer_than_output(paths, [outpath]):
        if not os.path.exists(os.path.split(outpath)[0]): 
            os.makedirs(os.path.split(outpath)[0])
        subs.to_csv(outpath, index=False)

    return subs

def read_sportseasondates():

    # read
    sportseasondates = pd.read_excel(
        "../data_raw/sport_season_startdates/sport_season_startdates.xlsx",
        parse_dates=["SPORT_SEASON_START_DT", "SPORT_SEASON_END_DT"],
        infer_datetime_format=True)
    
    # format col names
    sportseasondates.columns = [re.sub(r"\s", "_", c.strip().lower()) 
        for c in sportseasondates.columns]
    logging.debug("\nsportseasondates.head():\n" + str(sportseasondates.head()))

    return sportseasondates

def read_markatts():

    logging.debug("\nmarketing attributions reading started ...")

    # get file names from multiple refresh folders
    mkatt_files = list(filter(
        lambda p: "gold_utm_generator" in p.lower(),
        glob("../data_raw/marketing_attributions/" + "[0-9]"*8 
            + "/*.csv")
        ))
    print("\nmkatt_files = " + str(mkatt_files))

    # reading in files as dfs
    mkatt_dfs = []
    for p in mkatt_files:
        
        # read in file
        df = pd.read_csv(p)
        
        # format markatts col names
        df.columns = [re.sub(r"\s", "_", c.strip().lower()) for c in df.columns]

        # keep only "final_url" and "channel" cols
        df = df[["final_url", "channel"]]
    
        # lowercase and strip cols
        for c in df.columns:
            df[c] = df[c].str.lower().str.strip()

        # drop nulls and duplicate final_url rows
        df = df.dropna()
        df = df.drop_duplicates(["final_url"])
        
        # add to list
        mkatt_dfs.append(df)
        
    # concating dfs
    mkatt_df = pd.concat(mkatt_dfs, ignore_index=True).drop_duplicates(
        ["final_url"])
    print("\ndf_info_str(mkatt_df):\n\n" + df_info_str(mkatt_df))
    return mkatt_df

def read_vidmetrics():

    logging.debug("\nvidmetrics reading started ...")

    # aggregate raw video data if needed
    video_agg()
    
    # get the vidfile paths
    vidfilepaths = glob("../data_transformed/video_metrics/"
        "videometrics_datelevel_" + "[0-9]"*8 + "*.csv")
    logging.debug("\nvidfilepaths = " + str(vidfilepaths))
    
    # combine video metrics files and save
    vidmetrics = pd.concat([pd.read_csv(p) for p in vidfilepaths], 
        ignore_index=True)\
        .groupby(["date", "userid", "sportcode"]).sum().reset_index()
    logging.debug("\nvidmetrics info:\n\n" + df_info_str(vidmetrics))

    return vidmetrics

def read_vidmetrics_temp():
    
    vidmetrics = pd.read_csv("../temp/vidmetrics.csv")
    logging.debug("\nvidmetrics info:\n\n" + df_info_str(vidmetrics))

    return vidmetrics

def read_emailmetrics():
    
    logging.debug("\nemailmetrics reading started ...")

    # aggregate raw email events data if needed
    email_agg()

    eventcategories = ["click", "open", "sent"]
    return_dfs = []
    for eventcategory in eventcategories:
    
        # get the eventfile paths
        eventfilepaths = glob("../data_transformed/email_engagement/"
            + eventcategory + "_datelevel_" + "[0-9]"*8 + ".csv")
        logging.debug("\neventfilepaths = " + str(eventfilepaths))

        # combine video metrics files and save
        eventmetrics = pd.concat([pd.read_csv(p) for p in eventfilepaths], 
            ignore_index=True)
        return_dfs.append(eventmetrics)
        logging.debug("\n" + eventcategory + " metrics info:\n\n" 
            + df_info_str(eventmetrics))

    return tuple(return_dfs)


def prep_for_sport_after_datasources_loaded(sportcode, subs, sportseasondates, markatts, 
    vidmetrics, clicks, opens, sents):

    # clean up raw subs table
    subs = clean_subs(subs, sportcode)

    # add operational features
    subs = add_operational_features(subs, sportseasondates, sportcode)

    # add marketing attribution features
    subs = add_markatt_features(subs, markatts)

    # get date_to_season dict 
    # (needed ahead for integrating email and video cons. data)
    date_to_season = get_date_to_season_dict(subs, sportcode)

    # add video consumption features
    subs = add_video_features(vidmetrics, 
        subs=subs, date_to_season=date_to_season, sportcode=sportcode)

    # add email engagement features
    #
    # clicks
    subs = add_email_features(clicks, eventcategory="click", 
        subs=subs, date_to_season=date_to_season, sportcode=sportcode)
    #
    # opens
    subs = add_email_features(opens, eventcategory="open", 
        subs=subs, date_to_season=date_to_season, sportcode=sportcode)
    #
    # sents
    subs = add_email_features(sents, eventcategory="sent", 
        subs=subs, date_to_season=date_to_season, sportcode=sportcode)

    # For PGAT, prepend sport to sport_season_nm
    if sportcode == "PGAT":
        subs["sport_season_nm"] = "PGA TOUR LIVE Monthly ("\
            + subs["sport_season_nm"] + ")"

    # save as churn model input
    outpath = "../data_transformed/churn_input/churn_input_" + sportcode\
        + ".csv"
    if not os.path.exists(os.path.split(outpath)[0]):
        os.makedirs(os.path.split(outpath)[0])
    subs.to_csv(outpath, index=False)

    # print cleaned subs info
    logging.debug("\nchurn_input info:\n\n" + df_info_str(subs))

def clean_subs(subs, sportcode):
    """Clean up subs table to keep relevant columns and one subscriber row per season.

    Args:
        subs (pandas.DataFrame): the raw subs
        sportcode (str): sportcode of the sport we want to process

    Returns:
        subs (pandas.DataFrame): the cleaned subs
    """

    logging.debug("\nCleaning subs ...")

    # avoiding SettingWithCopy warning
    subs = subs.copy()

    # keep some columns
    #
    colskeep = ["USER_ID", "SUBSCRIPTION_START_DT", "CAMPAIGN_URL", "STATUS",
         "TOTAL_CHARGED_AMT", "SPORT_SEASON_NM", "MULTI_SPORT_IND"]
    if sportcode == "PGAT":
        colskeep += ["SPORT_TERM_NM"]
    subs = subs[colskeep]
    #
    # format col names
    subs.columns = [re.sub(r"\s", "_", c.strip().lower()) for c 
                      in subs.columns]

    # lower case of user_id for compatibility with other data sources
    subs["user_id"] = subs["user_id"].str.lower()

    # For sportcode PGAT,
    # 1. keep only monthly subscribers
    # 2. create a year-month col but call it sport_season_nm for compatibility
    # with forecast code
    #
    if sportcode == "PGAT":
        # get the monthly users using sport_term_nm
        subs = subs[
            subs["sport_term_nm"].str.contains("Monthly PGA TOUR LIVE")]
        subs.drop(columns = ["sport_term_nm"], inplace = True)
        #
        # keep only subs with realistic monthly subscription prices (under $15)
        subs = subs[subs["total_charged_amt"] < 15]
        #
        # get year-month as sport_season_nm
        subs["sport_season_nm"] = subs["subscription_start_dt"].astype(str)\
            .str[None:7]

    # remove "weird" seasons (partials etc.):
    # we simply remove seasons with unusual name lengths
    #
    # get lens of seasonames
    seasonnames = subs["sport_season_nm"].drop_duplicates().sort_values()
    seasonnamelens = pd.Series(seasonnames.str.len().values,
        index=seasonnames)
    # counts of the lens
    seasonnamelen_counts = seasonnamelens.value_counts()
    # do something only if we've got multiple lengths
    if len(seasonnamelen_counts) > 1:
        logging.debug("Found season names with different lengths. "
            "Will try to discard \"weird\" seasons with unusual lengths ...")
        # make sure there is a single most frequent length by comparing the top two
        if seasonnamelen_counts.iloc[0] == seasonnamelen_counts.iloc[1]:
            raise ValueError("Different length season names have equal numbers. "
                "Not sure which to discard. \nSeason-name-length counts:\n"
                + str(seasonnamelen_counts))
        # keep only seasonnamess with the most frequent length
        seasonnames_keep = seasonnames[
            (seasonnamelens == seasonnamelen_counts.index[0]).values].values
        subs = subs[subs["sport_season_nm"].isin(seasonnames_keep)]
        logging.debug("Removed seasons:" + str(set(seasonnames) - 
            set(seasonnames_keep)))
        logging.debug("Kept seasons:" + str(seasonnames_keep))

    # clean up subs to have only one row per user per season
    #
    # remove refunds
    subs = subs[~subs["status"].str.contains("REFUND")]
    subs.drop(columns=["status"], inplace=True)
    #
    # sort by season/date
    subs = subs.sort_values(by = ["sport_season_nm", 
        "subscription_start_dt"])
    #
    # drop duplicates within seasons
    subs = subs.drop_duplicates(["sport_season_nm", "user_id"])

    # For sportcode Moto, remove SXMXBundle first-season users from data as
    # they were intentionally encouraged to switch passes from Moto to SXMX
    #
    if sportcode == "Moto":
        logging.debug("Removing SXMXBundle users from analysis ...")
        #
        # read in bundle subs
        subs_bundle = pd.read_csv("../data_raw/subscriptions/SXMX.xlsx", 
            sep = "\t", parse_dates=["SUBSCRIPTION_START_DT"], 
            infer_datetime_format=True)
        #
        # format col names
        subs_bundle.columns = [re.sub(r"\s", "_", c.strip().lower()) for c 
            in subs_bundle.columns]
        #
        # lower case of user_id for compatibility with other data sources
        subs_bundle["user_id"] = subs_bundle["user_id"].str.lower()
        #
        # get uids in Bundle
        bundleuids = subs_bundle[subs_bundle["sport_season_nm"] == 
            subs_bundle["sport_season_nm"].min()]["user_id"].drop_duplicates()
        n_uids_before_removal = subs["user_id"].drop_duplicates().count()
        subs = subs[~subs["user_id"].isin(bundleuids)]
        n_uids_removed = n_uids_before_removal - subs["user_id"]\
            .drop_duplicates().count()
        logging.debug("n_uids_removed = " + str(n_uids_removed) 
            + ", n_uids_after_removal = " 
            + str(subs["user_id"].drop_duplicates().count()))

    return subs

def add_operational_features(subs, sportseasondates, sportcode):
    """Generate operational features related to time of subscription, price charged etc.

    Args:
        subs (pandas.DataFrame): the subs table
        sportseasondates (pandas.DataFrame): start and end dates of different sport-seasons
        sportcode (str): sportcode of the sport we want to process

    Returns:
        subs (pandas.DataFrame): subs table with operational feature columns added
    """

    logging.debug("\nAdding operational features ...")

    # season_days_elapsed
    #
    if sportcode != "PGAT":
        # get season start dates
        seasonstarts = sportseasondates[
            ["sport_season_nm", "sport_season_start_dt"]]
        seasonstarts.columns = ["sport_season_nm", "season_start_date"]
        subs = subs.merge(seasonstarts, how="left")
        #
        # get season_days_elapsed
        subs["season_days_elapsed"] = (subs["subscription_start_dt"] - 
            subs["season_start_date"]).dt.days

    # get tenure and will_churn:
    # tenure: number of consecutive seasons watched
    # will_churn: whether user will fail to renew in following season
    #
    # get seasons
    seasons = sorted(subs["sport_season_nm"].unique())
    #
    # seastenures will be a df that holds the tenure
    # of each user in each season that has occured. 
    # The rows will be the users. The columns will be the different seasons.
    # The value at <user> x <season> will be the tenure of <user>
    # in <season>.
    # - substartdates is similar but for subscription start date values
    # - churnflags is similar but for will_churn values.
    # - here we will only initialize the dfs with the row indices (user_ids)
    seastenures = pd.DataFrame(index=set(subs["user_id"].unique()))
    substartdates = pd.DataFrame(index=set(subs["user_id"].unique()))
    churnflags = pd.DataFrame(index=set(subs["user_id"].unique()))
    #
    # compute tenure, will_churn one season at a time
    for seas_iloc, seas in enumerate(seasons):
        # get subs from seas
        subs_seas = subs[subs["sport_season_nm"] == seas]
        #
        if sportcode == "PGAT":
            # get subscription_start_dts of users in seas
            substartdates[seas] = pd.to_datetime([None]*len(substartdates),
                infer_datetime_format=True)
            substartdates.loc[subs_seas["user_id"].values, seas] =\
                subs_seas["subscription_start_dt"].values
        #
        # set tenure of users in seas to 1
        seastenures[seas] = 0
        seastenures.loc[subs_seas["user_id"].values, seas] = 1
        #
        # set churnflag to 1 for all
        # change to 0 in next season, if appearing in time
        churnflags[seas] = 1
        #
        # update tenure for 2nd season and greater
        if seas_iloc > 0: 
            # get users from this season that exist in last season
            common_users_bool = (seastenures.iloc[:, seas_iloc] > 0)\
                & (seastenures.iloc[:, seas_iloc - 1] > 0)
            #
            # get users who renewed from last season in this season
            lastseasrenewedusers = common_users_bool.index[common_users_bool]
            # Have to consider sign-up date for PGAT because of rolling sub'n 
            # period
            if sportcode == "PGAT":
                # get this-season renewal deadlines (x days after last season 
                # start dates)
                lastseasstartdates = substartdates.iloc[
                    common_users_bool.values, seas_iloc - 1]
                daystorenew = 35
                thisseasrenewaldeadlines = lastseasstartdates + pd.Timedelta(
                    days=daystorenew)
                #
                # get renewed users
                renew_bools = substartdates.iloc[
                    common_users_bool.values, seas_iloc] \
                    <= thisseasrenewaldeadlines
                lastseasrenewedusers = renew_bools.index[renew_bools]
            #
            # for last-season renewed users
            # - set their churn flag in last season to False
            # - add their tenure from last season to this season
            churnflags.loc[lastseasrenewedusers, seasons[seas_iloc - 1]] = 0
            seastenures.loc[lastseasrenewedusers, seasons[seas_iloc]] +=\
                seastenures.loc[lastseasrenewedusers, seasons[seas_iloc - 1]]
    #
    # set last season churn flags to none
    churnflags.iloc[:, -1] = None
    #
    # get the tenures and churnflags into the subs table
    subs["tenure"] = subs.groupby("sport_season_nm")["user_id"]\
        .transform(lambda g: seastenures.loc[g.values, g.name])
    subs["will_churn"] = subs.groupby("sport_season_nm")["user_id"]\
        .transform(lambda g: churnflags.loc[g.values, g.name])

    # seasons watched
    #
    # set to 1 initially
    subs["seasons_watched"] = 1
    #
    # perform cumulative sum
    subs["seasons_watched"] = subs.groupby(["user_id"])["seasons_watched"]\
        .transform(np.cumsum)

    # price related
    #
    # price charged
    subs.rename(columns = {"total_charged_amt": "price_charged"}, 
                      inplace=True)
    #
    # remove users with price_charged <= 0
    subs = subs[subs["price_charged"] > 0]
    #
    # price charged first time (when subscriber joined)
    subs["price_charged_first_time"] = subs\
        .groupby("user_id")["price_charged"].transform("first")
    #
    # price charged now to first time ratio
    subs["price_charged_now_to_first_time"] = \
        subs["price_charged"] / subs["price_charged_first_time"]

    # multisport status
    subs["is_multisport"] = 0
    subs.loc[subs["multi_sport_ind"].str.contains("Y"), 
                   "is_multisport"] = 1
    subs.drop(columns=["multi_sport_ind"], inplace=True)

    return subs

def add_markatt_features(subs, markatts):
    """Generate marketing attribution features.

    Args:
        subs (pandas.DataFrame): the subs table
        markatts  (pandas.DataFrame): information regarding marketing campaigns

    Returns:
        subs (pandas.DataFrame): subs table with marketing attribution columns added
    """

    logging.debug("\nAdding marketing attribution features ...")

    # Fix marketing campaign urls: where null, assign most recent non-null
    #
    # seasmurls will be a df that holds the marketing url (murl) of each user
    # in each season (seas) that has occured. The rows will be the users. 
    # The columns will be the seasons. The values will be the murls.
    seasmurls = pd.DataFrame(index=set(subs["user_id"].unique()))
    for seas_ix, (seas, group) in enumerate(subs.groupby("sport_season_nm")):
        seasmurls[seas] = None
        seasmurls.loc[group["user_id"].values, seas] =\
            group["campaign_url"].values
        #
        # make "No Attribution" null
        seasmurls.loc[seasmurls[seas].str.contains("No Attribution").fillna(False), 
                      seas] = None
        #
        # get previous seas murl where there is no murl
        if seas_ix > 0:
            seasmurls[seas] = seasmurls[seas].mask(
                seasmurls[seas].isnull(), seasmurls.iloc[:, seas_ix - 1])
    #
    # get the new urls into the subs table
    subs["campaign_url"] = subs.groupby("sport_season_nm")["user_id"]\
        .transform(lambda g: seasmurls.loc[g.values, g.name].values)

    # format markatt colnames
    useful_cols = ["final_url", "channel"]
    markatts = markatts[useful_cols]
    colnewnames = {"final_url": "campaign_url"}
    markatts = markatts.rename(columns=colnewnames)
    markatts.columns = "mktg_" + markatts.columns.to_series()
    
    # join to subs
    #
    # lowercase and strip campaign_url col in subs for compatibility during join
    subs["campaign_url"] = subs["campaign_url"].str.lower().str.strip()
    #
    # rename campaign_url col for compatibility during join
    subs = subs.rename(columns={"campaign_url":"mktg_campaign_url"})
    #
    # join
    subs = subs.merge(markatts, how = "left")
    subs.drop(columns=["mktg_campaign_url"], inplace=True)

    # mktg_channel_exists: indicate users that have a mktg_channel
    subs["mktg_channel_exists"] = subs["mktg_channel"].notnull()*1

    # binarize mktg_channel
    #
    # binarize
    ohdf = pd.get_dummies(subs[["mktg_channel"]], ["mktg_channel"])
    #
    # keep only channels of interest
    channels_keep = ["email", "social", "web", "search", "pr"]
    ohdf_cols_keep = ohdf.columns[
        ohdf.columns.str.contains("|".join(channels_keep))]
    ohdf = ohdf[ohdf_cols_keep]
    #
    # merge with main subs
    subs = pd.concat([subs, ohdf], axis="columns")

    return subs

def get_date_to_season_dict(subs, sportcode):
    """Get a dict that maps any date to the season of the sport 
    containing it (None if one doesn't exist).
    """

    logging.debug("\nGetting date_to_season mapping ...")

    # mappinng from sport_season_nm to start date
    season_to_start = None
    if sportcode != "PGAT":
        season_to_start = subs[["sport_season_nm", "season_start_date"]]\
            .drop_duplicates().set_index("sport_season_nm").sort_index()
        season_to_start.columns = ["start_date"]
        season_to_start["start_date"] = pd.to_datetime(
            season_to_start["start_date"], infer_datetime_format=True)
    else: # sportcode == "PGAT"
        # not applicable
        date_to_season = None
        return date_to_season

    # mappinng from sport_season_nm to end date
    #
    season_to_start["end_date"] =\
        pd.to_datetime(season_to_start["start_date"].shift(-1))\
            - pd.DateOffset(days=1)
    #
    # end date for latest season must be computed based on its start date
    if sportcode != "PGAT":
        season_to_start.iloc[-1, -1] = season_to_start["start_date"].iloc[-1]\
            + pd.DateOffset(years=1) - pd.DateOffset(days=1)
    else: # sportcode == "PGAT"
        season_to_start.iloc[-1, -1] = season_to_start["start_date"].iloc[-1]\
            + pd.DateOffset(months=1) - pd.DateOffset(days=1)

    # map dates to seasons
    #
    # get dates in each season as a list of dfs
    season_to_dates_df_list = []
    for season in season_to_start.index:
        dates = pd.date_range(season_to_start.loc[season]["start_date"], 
            season_to_start.loc[season]["end_date"])
        season_to_dates = pd.DataFrame({"sport_season_nm": [season]*len(dates),
            "date": dates})
        season_to_dates_df_list.append(season_to_dates)

    # concat dfs and make dict from date to season
    date_to_season = defaultdict(lambda: None,
        pd.concat(season_to_dates_df_list, ignore_index=True)
            .set_index("date")["sport_season_nm"].to_dict()
        )
    return date_to_season

def add_video_features(vidmetrics, subs, date_to_season, sportcode):
    """Generate video consumption features including device and 
    live/replay information.

    Args:
        vidmetrics (pandas.DataFrame): table containing video consumption data
            on a (date, user, sportcode) level.
        subs (pandas.DataFrame): the subs table
        date_to_season (dict): mapping from date to season of sport
        sportcode (str): sportcode of the sport we want to process

    Returns:
        subs (pandas.DataFrame): subs table with video cons. columns added
    """

    logging.debug("\nAdding video features ...")

    # copy vidmetrics for modifying
    vidmetsagg = vidmetrics.copy()

    # renaming "userid" column for compatibility with subs
    vidmetsagg = vidmetsagg.rename(columns={"userid": "user_id"})

    # kicking out rows that don't correspond to current sport users
    sportusers = set(subs["user_id"].drop_duplicates())
    vidmetsagg = vidmetsagg[vidmetsagg["user_id"].isin(sportusers)]

    # keeping only rows for current and unknown sports 
    vidmetsagg = vidmetsagg[(vidmetsagg["sportcode"] == sportcode.lower())
        | (vidmetsagg["sportcode"] == "unknown")
        ]
    vidmetsagg = vidmetsagg.drop(["sportcode"], axis="columns")

    # converting date to season and summing to seasonuser level
    #
    # converting
    vidmetsagg, cons_lastdate = metricsdf_dateuserlevel_to_seasonuserlevel(
        vidmetsagg, date_to_season, subs, sportcode)

    # removing users that subscribed after last consumption date
    subs_drop_locs = subs["subscription_start_dt"] > cons_lastdate
    subs = subs[~subs_drop_locs]
    logging.debug("\nDropped " + str(subs_drop_locs.sum()) + " subs that came"
        + " in after last consumption date " + str(cons_lastdate))

    # recategorizing videosecs columns
    #
    # keeping track of original videosecs columns
    vidcolnames_orig = vidmetsagg.columns[
        vidmetsagg.columns.str.startswith("videosecs_")]
    #
    # video seconds in 
    # - total
    vidmetsagg["videosecs"] = vidmetsagg[vidcolnames_orig].sum(axis="columns")
    #
    # video seconds in
    # - connected
    # - mobile
    # - web
    # - live
    # - replay
    dims = ["connected", "mobile", "web", "live", "replay"]
    for dim in dims:
        dim_colnames = vidcolnames_orig[
            vidcolnames_orig.str.startswith("videosecs_") 
            & vidcolnames_orig.str.contains(dim)]
        vidmetsagg["videosecs_" + dim] = vidmetsagg[dim_colnames].sum(
            axis="columns")
    #
    # drop original videosec columns
    vidmetsagg = vidmetsagg.drop(columns = vidcolnames_orig)

    # join with subs
    #
    subs = subs.merge(vidmetsagg, how = "left")
    
    # normalizing by per-day consumption after user subscribed
    #
    vidcolnames = subs.columns[(subs.columns.str.startswith("videosecs"))]
    subs = norm_consumption_by_days_active(
        subs, vidcolnames, cons_lastdate, sportcode)

    # nan to 0
    subs[vidcolnames] = subs[vidcolnames].replace([np.nan, np.inf], 0)

    # major (most viewed) video platform
    #
    # specify device col names
    devicecolnames = ["videosecs_connected", "videosecs_mobile", "videosecs_web"]
    #
    # create major_video_platform col
    subs["major_video_platform"] = None
    subs.loc[subs["videosecs"] > 0, "major_video_platform"] =\
        subs.loc[subs["videosecs"] > 0, devicecolnames].idxmax(axis="columns")\
            .str[10:]

    return subs

def metricsdf_dateuserlevel_to_seasonuserlevel(metricsdf, date_to_season, 
    subs, sportcode):

    # convert date col to datetime format
    metricsdf["date"] = pd.to_datetime(metricsdf["date"].astype(str), 
        infer_datetime_format=True)
    #
    # saving last consumption date for returning
    cons_lastdate = metricsdf["date"][metricsdf["date"].notnull()].max()
    
    # map date to season
    #
    if sportcode != "PGAT":
        # map
        metricsdf["sport_season_nm"] = metricsdf["date"].map(date_to_season)
        #
        # remove rows with null sport_season_nm
        metricsdf = metricsdf[metricsdf["sport_season_nm"].notnull()]
        #
        # get mapping from season_user to subdt
        season_user_to_subdt = subs[["subscription_start_dt"]].copy()
        season_user_to_subdt["season_user"] = subs["sport_season_nm"] + "_"\
            + subs["user_id"]
        season_user_to_subdt = defaultdict(lambda: pd.to_datetime("nat"),
            season_user_to_subdt.drop_duplicates()\
                .set_index("season_user")["subscription_start_dt"]
            )
        #
        # for each user in each season, only keep consumption after subdt
        keep_rows = metricsdf["date"] >= (metricsdf["sport_season_nm"] 
            + "_" + metricsdf["user_id"]).map(season_user_to_subdt)
        metricsdf = metricsdf[keep_rows]
    #
    else: # sportcode == "PGAT"
        #
        # get user_consumptiondate to subscription_start_dt mapping
        user_condt_seas_table = get_user_consumptiondt_season_table_for_pgat(subs)
        user_condt_seas_table = user_condt_seas_table.rename(
            {"condt": "date"}, axis="columns")
        #
        # get sport_season_nm col in metricsdf
        metricsdf = metricsdf.merge(user_condt_seas_table, how="left")
        #
        # remove rows with null sport_season_nm
        metricsdf = metricsdf[metricsdf["sport_season_nm"].notnull()]

    # summing to season-user level
    #
    # dropping date
    metricsdf = metricsdf.drop(["date"], axis="columns")
    #
    # summing
    metricsdf = metricsdf.groupby(["sport_season_nm", "user_id"]).sum()\
        .reset_index()

    return metricsdf, cons_lastdate

def get_user_consumptiondt_season_table_for_pgat(subs):

    # get user to (subdt, sport_season_nm) mapping
    user_to_subdt = subs[["user_id", "subscription_start_dt", 
        "sport_season_nm"]].sort_values(["subscription_start_dt"])

    # get subdt to consumption dates mapping
    def get_subdt_to_condtsdf(subdt):
        # get consumption dates
        condts = pd.date_range(subdt, subdt + pd.to_timedelta("30 days"))
        #
        # put em in a subdt_to_condts df
        subdt_to_condts_df = pd.DataFrame({"condt": condts})
        subdt_to_condts_df["subscription_start_dt"] = subdt
        #
        return subdt_to_condts_df[["subscription_start_dt", "condt"]]
    subdts = user_to_subdt["subscription_start_dt"].drop_duplicates()
    subdt_to_condt = pd.concat(subdts.map(get_subdt_to_condtsdf).values,
        ignore_index=True)

    # merge and get user_condt to season mapping
    user_condt_seas_table = user_to_subdt.merge(subdt_to_condt)
    user_condt_seas_table = user_condt_seas_table[["user_id", "condt", 
        "sport_season_nm"]].drop_duplicates(["user_id", "condt"], 
            keep="last")
    return user_condt_seas_table

def norm_consumption_by_days_active(subs, metcolnames, cons_lastdate, sportcode):
    
    if sportcode != "PGAT":
        for season in subs["sport_season_nm"].drop_duplicates().values:
            #
            days_in_season = 365
            days_of_cons_max = 365
            if season == subs["sport_season_nm"].max():
                # fewer days_of_cons_max for last season
                days_of_cons_max = (cons_lastdate
                    - subs[subs["sport_season_nm"] == season]
                        ["season_start_date"].iloc[0]).days + 1
                days_of_cons_max = max(days_of_cons_max, 0)
            #
            user_days_of_cons = days_of_cons_max - subs["season_days_elapsed"] + 1
            for cn in metcolnames:
                subs.loc[subs["sport_season_nm"] == season, cn] =\
                    subs.loc[subs["sport_season_nm"] == season, cn]\
                        / user_days_of_cons * days_in_season
    
    else: # sportcode == "PGAT"
        # For PGAT, only those users that have joined less than a month before the
        # last consumption date have to be addressed. The rest have all had a full
        # month's (season's) consumption time.
        #
        days_in_season = 31
        #
        user_days_of_cons = (cons_lastdate - subs["subscription_start_dt"]).dt.days + 1
        user_days_of_cons = user_days_of_cons.mask(user_days_of_cons < 0, 0)
        #
        latestusers_sublocs = user_days_of_cons < days_in_season
        #
        for cn in metcolnames:
            subs.loc[:, cn] = subs[cn].mask(
                latestusers_sublocs, 
                subs[cn] / user_days_of_cons * days_in_season)

    return subs

def add_email_features(eventsdf, eventcategory, subs, 
    date_to_season, sportcode):
    """Generate email event features including "email intent" information e.g.
    "emailclicks_eng", "emailclicks_acq"

    Args:
        eventsdf (pandas.DataFrame): table containing email events 
            (e.g. clicks) aggregated to a month-user level.
        eventcategory (str): the event category e.g. clicks
        subs (pandas.DataFrame): the subs table
        date_to_season (dict): mapping from date to season of sport
        sportcode (str): sportcode of the sport we want to process

    Returns:
        subs (pandas.DataFrame): subs table with email events columns added
    """

    logging.debug("\nAdding email " + eventcategory + " features ...")

    # avoiding setting values on a dataframe-slice-copy (SettingWithCopyWarning)
    eventsdf = eventsdf.copy()

    # drop subscriberkey
    eventsdf = eventsdf.drop(["subscriberkey"], axis="columns")

    # rename pianoid to user_id
    eventsdf = eventsdf.rename(columns={"pianoid": "user_id"})

    # kicking out rows that don't correspond to current sport users
    sportusers = set(subs["user_id"].drop_duplicates())
    eventsdf = eventsdf[eventsdf["user_id"].isin(sportusers)]

    # keep only rows with sportcode = (sportcode or "multi")
    eventsdf = eventsdf[(eventsdf["sportcode"] == sportcode.lower())
        | (eventsdf["sportcode"] == "multi")
        ]
    eventsdf = eventsdf.drop(["sportcode"], axis="columns")

    # keep only rows with brand = "gold"
    eventsdf = eventsdf[(eventsdf["brand"].str.contains("gold"))]
    eventsdf = eventsdf.drop(["brand"], axis="columns")

    # getting separate cols for each "type" ("eng", "acq", "acc_not")
    #
    # replacing "acc_not" with "other" in type
    eventsdf["type"] = eventsdf["type"].replace("acc_not", "other")
    #
    # getting separate cols
    types = ["eng", "acq", "other"]
    for typ in types:
        eventsdf["email" + eventcategory + "s_" + typ] = 0
        eventsdf["email" + eventcategory + "s_" + typ] =\
            eventsdf["email" + eventcategory + "s_" + typ].mask(
            eventsdf["type"] == typ, eventsdf["n_events"])
    eventsdf = eventsdf.drop(["n_events", "type"], axis="columns")

    # converting date to season and summing to seasonuser level
    eventsdf, cons_lastdate = metricsdf_dateuserlevel_to_seasonuserlevel(
        eventsdf, date_to_season, subs, sportcode)

    # removing users that subscribed after last consumption date
    subs_drop_locs = subs["subscription_start_dt"] > cons_lastdate
    subs = subs[~subs_drop_locs]
    logging.debug("\nDropped " + str(subs_drop_locs.sum()) + " subs that came"
        + " in after last consumption date " + str(cons_lastdate))
    
    # merge
    subs = subs.merge(eventsdf, how="left")
 
    # normalizing by per-day consumption after user subscribed
    eventcolnames = subs.columns[subs.columns.str.startswith(
        "email" + eventcategory + "s_")]
    subs = norm_consumption_by_days_active(
        subs, eventcolnames, cons_lastdate, sportcode)

    # nan to 0
    subs[eventcolnames] = subs[eventcolnames].replace([np.nan, np.inf], 0)
    
    return subs

if __name__ == "__main__":
    configure_logging("DEBUG")

    churn_data_prep(["Track&Field"])

    # sportcodes = ["PGAT", "PL", "Cycling", "Moto", "Rugby", "Track&Field"]
    # churn_data_prep(sportcodes)