import os
import numpy as np

def find_if_input_files_newer_than_output(infiles, outfiles):

    # ensure both args are lists
    assert (isinstance(infiles, list) and isinstance(outfiles, list)),\
        "Each argument to find_if_input_files_newer_than_output() "\
            "must be a list of paths."
    
    # get modified intimes
    intimes = np.array(list(map(os.path.getmtime, infiles)))

    # assume output newer than input
    in_newer_than_out = False
    
    # get modified outtimes
    outtimes = []
    for outfile in outfiles:
        if os.path.exists(outfile):
            outtimes.append(os.path.getmtime(outfile))
        else:
            in_newer_than_out = True
            return in_newer_than_out
    outtimes = np.array(outtimes)

    for intime in intimes:
        if np.any(intime > outtimes):
            in_newer_than_out = True
            return in_newer_than_out

    return in_newer_than_out