import logging
from configure_logging import configure_logging
from glob import glob
from dask.distributed import Client
from dask_read_csv import dask_read_csv
from df_info_str import df_info_str
from find_if_input_files_newer_than_output import\
    find_if_input_files_newer_than_output
import os
import pandas as pd
from collections import defaultdict

def video_agg(delete_extracts_when_done=True):

    # note start time
    start_time = pd.Timestamp.now()
    logging.info("\nVideo agg started at " + str(start_time))

    # get refresh folder names
    refreshfolders = list(sorted(glob(
        "../data_raw/video_metrics/" + "[0-9]"*8)))
    logging.debug("\nrefreshfolders = " + str(refreshfolders))

    # agg refresh folders sequentially, and over sweeps
    sweeps = ["gold", "omni"]
    for ix, refreshfolder in enumerate(refreshfolders[None:None]):
        previousrefreshfolder = None
        if ix > 0:
            previousrefreshfolder = refreshfolders[ix - 1]
        for sweep in sweeps:
            agg_refreshfolder_sweep(refreshfolder, sweep, previousrefreshfolder,
                delete_extracts_when_done)

    # note time taken
    logging.info("\nVideo agg done in " + str(pd.Timestamp.now() - start_time))

def agg_refreshfolder_sweep(refreshfolder, sweep, previousrefreshfolder, 
    delete_extracts_when_done):
    
    logging.debug("\nAgging started for refreshfolder = " + refreshfolder 
        + ", sweep = " + sweep + " ...")
    
    # get sweep files in folder
    filenamepart = {"gold": "goldvideo", "omni": "omnitureliveextra"}[sweep]
    sweepfiles = list(filter(lambda p: filenamepart in p.lower(),
        glob(refreshfolder + "/*.tsv") + glob(refreshfolder + "/*.tsv.gz")))
    logging.debug("\nsweepfiles = " + str(sweepfiles))

    # return if no sweepfiles
    if len(sweepfiles) == 0:
        logging.debug("\nNothing to agg for refreshfolder = " + refreshfolder 
        + ", sweep = " + sweep)
        return

    # return if input files are older than agged results
    aggpath = aggpath_for_refreshfolder(refreshfolder, sweep)
    if not find_if_input_files_newer_than_output(sweepfiles, 
        [aggpath]):
        logging.debug("\nNothing new to agg for refreshfolder = " + refreshfolder 
        + ", sweep = " + sweep)
        return

    # agg the sweep files
    sep = "\t"
    getrelevantcolumns_func = {"gold": getrelevantcolumns_gold, 
        "omni": getrelevantcolumns_omni}[sweep]
    agged_dfs = []
    for filepath in sweepfiles:
        agged_dfs.append(agg_file(filepath, sep, getrelevantcolumns_func,
            delete_extracts_when_done))
    
    # join and agg the agged dfs
    df_agged = agged_dfs[0]
    if len(agged_dfs) > 1:
        df_agged = agg_ddf(pd.concat(agged_dfs, ignore_index=True))

    # drop last date data (due to incompleteness of data on that date)
    df_agged = df_agged[df_agged["date"] < df_agged["date"].max()]
    
    # remove overlapping dates with previous refresh
    if previousrefreshfolder is not None:
        previousrefreshaggpath = aggpath_for_refreshfolder(
            previousrefreshfolder, sweep)
        if os.path.exists(previousrefreshaggpath):
            prevref_maxdate = str(
                pd.read_csv(previousrefreshaggpath)["date"].max())
            logging.debug("\nprevref_maxdate = " + prevref_maxdate)
            df_agged = df_agged[df_agged["date"] > prevref_maxdate]

    # save agg
    if not os.path.exists(os.path.split(aggpath)[0]):
        os.makedirs(os.path.split(aggpath)[0])
    df_agged.to_csv(aggpath, index=False)
    logging.debug("\n" + aggpath + " info:\n\n" + df_info_str(df_agged))
    logging.debug("\nAgging done for refreshfolder = " + refreshfolder 
        + ", sweep = " + sweep + " ...")

def aggpath_for_refreshfolder(refreshfolder, sweep):
    refreshdate = os.path.split(refreshfolder)[1]
    return "../data_transformed/video_metrics/videometrics_datelevel"\
        + "_" + refreshdate + "_" + sweep + ".csv"

def agg_file(filepath, sep, getrelevantcolumns_func, delete_extracts_when_done):

    # note start time
    start_time = pd.Timestamp.now()
    logging.debug("\nAgging " + filepath + " started at " + str(start_time))

    # start dask client
    daskclient = Client(processes=False)
    logging.debug("\ndaskclient = " + str(daskclient))

    # read file into ddf
    ddf, extractedfilepath = dask_read_csv(filepath, sep, 
        partitionsize="64MB")
    
    # get sportraw to sportcode mapping (needed for agg)
    sportraw_to_sportcode = get_sportraw_to_sportcode(extractedfilepath, 
        sep, getrelevantcolumns_func)
    
    # agg the file
    df_agged = agg_ddf(ddf, getrelevantcolumns_func, 
        sportraw_to_sportcode).compute()

    # close dask client
    daskclient.close()

    # delete extracted file if needed
    if delete_extracts_when_done and "extracted_gzip_cache" in extractedfilepath:
        try: os.remove(extractedfilepath)
        except: logging.debug("\nCound not delete " + extractedfilepath)

    # return
    logging.debug("\nAgging " + filepath + " done in " 
        + str(pd.Timestamp.now() - start_time))
    return df_agged

def getrelevantcolumns_gold(ddf):

    # avoid SettingWithCopyWarning
    ddf = ddf.copy()

    # standardize colnames
    ddf.columns = ddf.columns.str.replace(" ", "")\
        .str.lower()

    # keep relevant columns
    colnames_keep = ["date", "useridevar4", "videosportclean", 
        "mobiledevicetype", "livestreamsecondswatched", "replaysecondsspent"]
    colnames_new = ["date", "userid", "sport", "devicetype", "livesecs", 
        "replaysecs"]
    ddf = ddf[colnames_keep]
    ddf.columns = colnames_new
    return ddf

def getrelevantcolumns_omni(ddf):

    # avoid SettingWithCopyWarning
    ddf = ddf.copy()

    # standardize colnames
    ddf.columns = ddf.columns.str.replace(" ", "")\
        .str.lower()

    # keep relevant columns
    #
    # initialize colnames_keep
    colnames_keep = []
    #
    # add date column
    colnames_keep.append("date")
    #
    # find userid column
    colnames_keep.append(ddf.columns[ddf.columns.str.contains("userid")][0])
    #
    # find sport column
    colnames_keep.append(ddf.columns[ddf.columns.str.contains("videosport")][0])
    #
    # find devicetype column
    colnames_keep.append(
        ddf.columns[ddf.columns.str.contains("devicetype")][0])
    #
    # find livesecs column
    colnames_keep.append(
        ddf.columns[
            ddf.columns.str.contains("live")\
            & ddf.columns.str.contains("seconds")
            ][0])
    #
    # find replaysecs column
    colnames_keep.append(
        ddf.columns[
            ddf.columns.str.contains("fer")\
            & ddf.columns.str.contains("seconds")
            ][0])
    #
    # set new colnames
    colnames_new = ["date", "userid", "sport", "devicetype", "livesecs", 
        "replaysecs"]
    ddf = ddf[colnames_keep]
    ddf.columns = colnames_new
    return ddf

def get_sportraw_to_sportcode(filepath, sep, 
    getrelevantcolumns_func):

    # read in a sample of (max) 1 million rows to get the mappings
    events_sample = pd.read_csv(filepath,
        sep=sep,
        nrows=10**6,
        dtype="object",
        error_bad_lines=False,
        )
    sample_clean = getrelevantcolumns_func(events_sample)
    sample_clean["sport"] = sample_clean["sport"].str.lower()

    # map sport to subscriptions-sportcode
    #
    # sportnamepart to sportcode mapping
    sportnamepart_to_sportcode = defaultdict(lambda: "unknown", {
        "pga": "pgat",
        "golf": "pgat",
        "cycling": "cycling",
        "soccer": "pl",
        "premier-league": "pl",
        "motocross": "moto",
        "-moto": "moto",
        "rugby": "rugby",
        "figure skating": "figureskating",
        "track and field": "track&field",
        "snow": "snow",
        "philly": "philly",
        "blazers": "blazers",
        "speed skating": "speedskating",
        "lacrosse": "pll"
        })
    sportnameparts = pd.Series(list(sportnamepart_to_sportcode))
    #
    # function to get the matching namepart from a name
    def contained_namepart(name, nameparts):
        match = None
        matchflags = nameparts.map(lambda namepart: namepart in name)
        if True in matchflags.values:
            match = nameparts[matchflags].iloc[0]
        return match
    #
    # series mapping from raw "sport" to sportcode
    sportraw_to_sportcode_ser = sample_clean.groupby("sport")["sport"].apply(
        lambda g: sportnamepart_to_sportcode[
            contained_namepart(g.name, sportnameparts)]
        )
    sportraw_to_sportcode = defaultdict(lambda: "unknown",
        sportraw_to_sportcode_ser[sportraw_to_sportcode_ser != "unknown"]
            .to_dict()
        )
    return sportraw_to_sportcode

def agg_ddf(event_ddf, getrelevantcolumns_func=None, 
    sportraw_to_sportcode=None):

    # avoid SettingWithCopyWarning
    event_ddf = event_ddf.copy()

    if "sportcode" not in event_ddf.columns:
    	# ddf is "raw"
    
        # clean event_ddf
        event_ddf = getrelevantcolumns_func(event_ddf)

        # lowercase and strip string cols
        strcols = ["sport", "devicetype", "userid"]
        for cn in strcols:
            event_ddf[cn] = event_ddf[cn].str.lower().str.strip()

        # get sportcode from sport
        event_ddf["sportcode"] = event_ddf["sport"].map(sportraw_to_sportcode)
        event_ddf["sportcode"] = event_ddf["sportcode"].fillna("unknown")
        event_ddf = event_ddf.drop(["sport"], axis="columns")

        # transforming devicetypes to business devicetypes: 
        # mobile, connected, web
        devicetype_to_businessdevicetype = defaultdict(lambda: "web", {
            "other": "web",
            "mobile phone": "mobile",
            "set-top box": "connected",
            "tablet": "mobile",
            "nan": "web",
            "television": "connected",
            "gaming console": "connected",
            "media player": "web",
            })
        event_ddf["devicetype"] = event_ddf["devicetype"]\
            .map(devicetype_to_businessdevicetype)

        # numerizing devicetype
        #
        # numerizing videoseconds columns
        event_ddf["livesecs"] = event_ddf["livesecs"].astype(float)
        event_ddf["replaysecs"] = event_ddf["replaysecs"].astype(float)
        #
        # getting live and replay consumption columns for each devicetype
        devicetypes = set(devicetype_to_businessdevicetype.values())
        for dt in devicetypes:
            dt_used_bool_col = (event_ddf["devicetype"] == dt)
            event_ddf["videosecs_replay_devicetype_" + dt] =\
                dt_used_bool_col * event_ddf["replaysecs"]
            event_ddf["videosecs_live_devicetype_" + dt] =\
                dt_used_bool_col * event_ddf["livesecs"]
        #
        # dropping original devicetype and videoform columns
        event_ddf = event_ddf.drop(["devicetype", "livesecs", "replaysecs"],
            axis="columns")

    # summing to (date, userid, sportcode) level
    event_ddf = event_ddf.groupby(["date", "userid", "sportcode"]).sum()\
        .reset_index()

    return event_ddf

if __name__ == "__main__":
    configure_logging("DEBUG")
    video_agg(delete_extracts_when_done=False)